#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

#include <GL/glew.h>
#include <GL/GL.h>
#include <SDL2/SDL.h>

#include "OGLDebug.h"
#include "scene.h"
#include "CS300Parser.h"


static int     winID;
static GLsizei WIDTH = 1280;
static GLsizei HEIGHT = 720;


GLuint Scene::CreateShader(GLenum eShaderType, const std::string& strShaderFile)
{
	GLuint       shader = glCreateShader(eShaderType);
	const char* strFileData = strShaderFile.c_str();
	glShaderSource(shader, 1, &strFileData, NULL);

	glCompileShader(shader);

	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{
		GLint infoLogLength;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);

		GLchar* strInfoLog = new GLchar[infoLogLength + 1];
		glGetShaderInfoLog(shader, infoLogLength, NULL, strInfoLog);

		const char* strShaderType = NULL;
		switch (eShaderType)
		{
		case GL_VERTEX_SHADER:
			strShaderType = "vertex";
			break;
		case GL_GEOMETRY_SHADER:
			strShaderType = "geometry";
			break;
		case GL_FRAGMENT_SHADER:
			strShaderType = "fragment";
			break;
		}

		fprintf(stderr, "Compile failure in %s shader:\n%s\n", strShaderType, strInfoLog);
		delete[] strInfoLog;
	}

	return shader;
}

GLuint Scene::CreateProgram(const std::vector<GLuint>& shaderList)
{
	GLuint program = glCreateProgram();

	for (size_t iLoop = 0; iLoop < shaderList.size(); iLoop++)
		glAttachShader(program, shaderList[iLoop]);

	glLinkProgram(program);

	GLint status;
	glGetProgramiv(program, GL_LINK_STATUS, &status);
	if (status == GL_FALSE)
	{
		GLint infoLogLength;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);

		GLchar* strInfoLog = new GLchar[infoLogLength + 1];
		glGetProgramInfoLog(program, infoLogLength, NULL, strInfoLog);
		fprintf(stderr, "Linker failure: %s\n", strInfoLog);
		delete[] strInfoLog;
	}

	for (size_t iLoop = 0; iLoop < shaderList.size(); iLoop++)
		glDetachShader(program, shaderList[iLoop]);

	return program;
}


/**
* @brief Initializes the provided shader
*
* @param texture: whether the program uses the texture shaders
* @param program: the program (for shapes or normals) to give the shader to
*
*/
void Scene::InitializeShader(bool texture, GLuint& program)
{
	//Get the shaders used for textures or colors
	std::vector<GLuint> shaderList;
	if (texture)
	{
		shaderList.push_back(CreateShader(GL_VERTEX_SHADER, shaders.strVertexShader_Texture));
		shaderList.push_back(CreateShader(GL_FRAGMENT_SHADER, shaders.strFragmentShader_Texture));
	}
	else
	{
		shaderList.push_back(CreateShader(GL_VERTEX_SHADER, shaders.strVertexShader_Color));
		shaderList.push_back(CreateShader(GL_FRAGMENT_SHADER, shaders.strFragmentShader_Color));
	}

	//Create the program
	program = CreateProgram(shaderList);

	std::for_each(shaderList.begin(), shaderList.end(), glDeleteShader);
}


void Scene::InitializeProgram()
{
	InitializeShader(using_texture, theProgram_S);

	if(draw_normals)
		InitializeShader(false, theProgram_N);

	//Create the arrays of VAOs and VBOs
	AO_S.clear();
	AO_N.clear();
	for (int i = 0; i < parser.objects.size(); i++)
	{
		AO_S.push_back(*new ArraysObject);
		AO_N.push_back(*new ArraysObject);
	}

	//Enable all the culling, backface removal...
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glEnable(GL_DEPTH_TEST);
	glFrontFace(GL_CCW);
	glDepthFunc(GL_LESS);
	glDepthRange(0.f, 1.f);

}

void Scene::InitializeBuffers()
{
	int max_obj = parser.objects.size();
	for (int i = 0; i < max_obj; i++)
	{
		const char* mesh_name = parser.objects[i].mesh.c_str();

		std::vector<Vertex> vtx = shape.DrawShape(parser.objects[i].mesh, change_subdiv);
		vtx_count += vtx.size();

		// VAO - SHAPES
		{
			glGenVertexArrays(1, &AO_S[i].vao);
			glBindVertexArray(AO_S[i].vao);



			// VBO for positions
			glGenBuffers(1, &AO_S[i].BufferObjects[0]);

			glBindBuffer(GL_ARRAY_BUFFER, AO_S[i].BufferObjects[0]);
			glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vtx.size(), vtx.data(), GL_STATIC_DRAW);

			// Insert the VBO into the VAO
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);


			// VBO for colors
			glGenBuffers(1, &AO_S[i].BufferObjects[1]);

			glBindBuffer(GL_ARRAY_BUFFER, AO_S[i].BufferObjects[1]);
			glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vtx.size(), vtx.data(), GL_STATIC_DRAW);

			glEnableVertexAttribArray(1);
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, color)));


			// VBO for UVs
			glGenBuffers(1, &AO_S[i].BufferObjects[2]);

			glBindBuffer(GL_ARRAY_BUFFER, AO_S[i].BufferObjects[2]);
			glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vtx.size(), vtx.data(), GL_STATIC_DRAW);

			// Insert the VBO into the VAO
			glEnableVertexAttribArray(2);
			glVertexAttribPointer(2, 2, GL_UNSIGNED_BYTE, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, UV)));

		}
		
		// VAO - NORMALS
		if(draw_normals)
		{
			std::vector<Vertex> normals;
			
			//Average normals
			if (average_normals)
				normals = shape.GetNormals(vtx, true);
			//Face normals
			else
				normals = shape.GetNormals(vtx, false);
			
			normals_count += normals.size();

			glGenVertexArrays(1, &AO_N[i].vao);
			glBindVertexArray(AO_N[i].vao);



			// VBO for positions
			glGenBuffers(1, &AO_N[i].BufferObjects[0]);

			glBindBuffer(GL_ARRAY_BUFFER, AO_N[i].BufferObjects[0]);
			glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * normals.size(), normals.data(), GL_STATIC_DRAW);

			// Insert the VBO into the VAO
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, pos)));


			// VBO for colors
			glGenBuffers(1, &AO_N[i].BufferObjects[1]);

			glBindBuffer(GL_ARRAY_BUFFER, AO_N[i].BufferObjects[1]);
			glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * normals.size(), normals.data(), GL_STATIC_DRAW);

			glEnableVertexAttribArray(1);
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, color)));

		}
	}

}


/**
* @brief Updates the camera view matrix after getting input from the user
*
* @return: the new view matrix
*/
glm::mat4 Scene::UpdateCameraViewMtx()
{
	//Get the position of the camera (will need to be offsetted by the target
	glm::vec3 pos{	cam_radius * sinf(alpha) * sinf(beta),
					cam_radius * cosf(alpha),
					cam_radius * sinf(alpha) * cosf(beta)};

	//Get the view matrix
	glm::mat4 View = glm::lookAt(pos + parser.camTarget, parser.camTarget, parser.camUp);

	return View;

}

/**
* @brief Create the transformation matrices for each object
*
*/
void Scene::TransfMatrices()
{
	int TOTAL_obj = parser.objects.size();
	for (int i = 0; i < TOTAL_obj; i++)
	{
		CS300Parser::Transform transf = parser.objects[i];

		//Create the matrices for the transformation with data from the parser
		glm::mat4 T = glm::translate(glm::mat4(1.f), glm::vec3(transf.pos.x, transf.pos.y, transf.pos.z));
		glm::mat4 Rx = glm::rotate(glm::mat4(1.f), glm::radians(transf.rot.x), glm::vec3(1.f, 0.f, 0.f));
		glm::mat4 Ry = glm::rotate(glm::mat4(1.f), glm::radians(transf.rot.y), glm::vec3(0.f, 1.f, 0.f));
		glm::mat4 Rz = glm::rotate(glm::mat4(1.f), glm::radians(transf.rot.z), glm::vec3(0.f, 0.f, 1.f));
		glm::mat4 R = Rz * Ry * Rx;
		glm::mat4 S = glm::scale(glm::mat4(1.f), glm::vec3(transf.sca.x, transf.sca.y, transf.sca.z));

		//Create the transformation matrix of the current object
		glm::mat4 transf_mtx = T * R * S;
		transform.push_back(transf_mtx);
	}
}

/**
* @brief Get keyboard input from the user
*
*/
void Scene::GetInput(SDL_Event event)
{
	//Quit the program
	if (event.key.keysym.scancode == SDL_SCANCODE_ESCAPE)
		quit = true;

	//Change the number of subdivisions of the shapess
	else if (event.key.keysym.scancode == SDL_SCANCODE_KP_PLUS)
	{
		change_subdiv = increase;
		update();
	}
	else if (event.key.keysym.scancode == SDL_SCANCODE_KP_MINUS)
	{
		change_subdiv = decrease;
		update();
	}

	//Change the camera's vertical angle
	else if (event.key.keysym.scancode == SDL_SCANCODE_W)
	{
		if (alpha > 0.06f && alpha < 3.1f)
			alpha -= 0.05f;
	}
	else if (event.key.keysym.scancode == SDL_SCANCODE_S)
	{
		if (alpha >= 0.f && alpha < 3.06f)
			alpha += 0.05f;
	}

	//Change the camera's horizontal angle
	else if (event.key.keysym.scancode == SDL_SCANCODE_A)
	{
		beta -= 0.05f;
	}
	else if (event.key.keysym.scancode == SDL_SCANCODE_D)
	{
		beta += 0.05f;
	}

	//Change the camera's radius
	else if (event.key.keysym.scancode == SDL_SCANCODE_E)
	{
		if (cam_radius > 0 && cam_radius <= max_cam_radius)
			cam_radius += 1.f;
	}
	else if (event.key.keysym.scancode == SDL_SCANCODE_Q)
	{
		if (cam_radius > 0 && cam_radius <= max_cam_radius)
			cam_radius -= 1.f;
	}

	//Change to using texture or color
	else if (event.key.keysym.scancode == SDL_SCANCODE_T)
	{
		using_texture = !using_texture;
		update();
	}

	//See shapes in wireframe or full mode
	else if (event.key.keysym.scancode == SDL_SCANCODE_M)
	{
		wireframe_mode = !wireframe_mode;
	}

	//Draw the normals
	else if (event.key.keysym.scancode == SDL_SCANCODE_N)
	{
		draw_normals = !draw_normals;
		update();
	}

	//Change from average normals to face normals
	else if (event.key.keysym.scancode == SDL_SCANCODE_F)
	{
		average_normals = !average_normals;
		update();
	}
}



//Called after the window and OpenGL are initialized. Called exactly once, before the main loop.
void Scene::init()
{
	//Load the parser
	parser.LoadDataFromFile("input.txt");

	//Camera info
	cam_radius = parser.camTarget.z - parser.camPos.z;
	max_cam_radius = cam_radius * 2.5f;
	alpha = 1.55f;
	beta = 0.f;

	//Set the projection matrix only once
	Projection = glm::perspective(glm::radians(parser.fovy), parser.width / parser.height, parser.nearPlane, parser.farPlane);

	//Initialize everything
	InitializeProgram();
	InitializeBuffers();
	TransfMatrices();

	//Set the texture generated procedurally
	texture.LoadTexture();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture.texture_);

	GLuint loc = glGetUniformLocation(theProgram_S, "textureData");
	glUniform1i(loc, 0);
}

//Called to update the scene
void Scene::update()
{
	//Reset
	vtx_count = 0;
	normals_count = 0;

	InitializeProgram();
	InitializeBuffers();

	change_subdiv = keep;
}

//Called to update the display.
//You should call SDL_GL_SwapWindow after all of your rendering to display what you rendered.
void Scene::display(SDL_Window* window)
{
	//Clear
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// Bind the glsl program and this object's VAO
	glUseProgram(theProgram_S);
	InitializeShader(using_texture, theProgram_S);


	//Set the wireframe or full mode
	if(wireframe_mode)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	//Get the updated view matrix
	glm::mat4 View = UpdateCameraViewMtx();
	std::vector<glm::mat4> MVP;

	//Bind the MVPs for each shape
	for (int i = 0; i < parser.objects.size(); i++)
	{
		MVP.push_back(Projection * View * transform[i]);
		
		//Draw the shape
		{
			//The vao that contains the shapes
			glBindVertexArray(AO_S[i].vao);

			//Set the uniform matrix MVP
			GLuint unif_loc_shape = glGetUniformLocation(theProgram_S, "MVP");
			glUniformMatrix4fv(unif_loc_shape, 1, GL_FALSE, &(MVP[i])[0][0]);

			// Draw
			glDrawArrays(GL_TRIANGLES, 0, vtx_count);

			// Unbind
			glBindVertexArray(0);
		}
	}



	if (draw_normals)
	{
		//Change the program and the shader for drawing the normals
		// Bind the glsl program and this object's VAO
		glUseProgram(theProgram_N);
		InitializeShader(false, theProgram_N);

		
		//Draw the normals
		for (int i = 0; i < parser.objects.size(); i++)
		{
			//The vao that contains the normals
			glBindVertexArray(AO_N[i].vao);

			//Set the uniform matrix MVP
			GLuint unif_loc = glGetUniformLocation(theProgram_N, "MVP");
			glUniformMatrix4fv(unif_loc, 1, GL_FALSE, &(MVP[i])[0][0]);

			// Draw
			glDrawArrays(GL_LINES, 0, normals_count);

			// Unbind
			glBindVertexArray(0);
		}
	}

	glUseProgram(0);

	SDL_GL_SwapWindow(window);
}

//Called to clean up the scene befpre every update and at the end of the program
void Scene::cleanup()
{
	// Delete the program
	glDeleteProgram(theProgram_S);
	glDeleteProgram(theProgram_N);

	for (int i = 0; i < parser.objects.size(); i++)
	{
		// Delete the VBOs
		glDeleteBuffers(1, AO_S[i].BufferObjects);
		glDeleteBuffers(1, AO_N[i].BufferObjects);

		// Delete the VAO
		glDeleteVertexArrays(1, &AO_S[i].vao);
		glDeleteVertexArrays(1, &AO_N[i].vao);
	}
}



#undef main
int main(int argc, char* args[])
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		std::cout << "Could not initialize SDL: " << SDL_GetError() << std::endl;
		exit(1);
	}

	SDL_Window* window = SDL_CreateWindow("CS300", 100, 100, WIDTH, HEIGHT, SDL_WINDOW_OPENGL);
	if (window == nullptr)
	{
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		exit(1);
	}

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	SDL_GLContext context_ = SDL_GL_CreateContext(window);
	if (context_ == nullptr)
	{
		SDL_DestroyWindow(window);
		std::cout << "SDL_GL_CreateContext Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		exit(1);
	}

	glewExperimental = true;
	if (glewInit() != GLEW_OK)
	{
		SDL_GL_DeleteContext(context_);
		SDL_DestroyWindow(window);
		std::cout << "GLEW Error: Failed to init" << std::endl;
		SDL_Quit();
		exit(1);
	}

#if _DEBUG
	glEnable(GL_DEBUG_OUTPUT);
	glDebugMessageCallback(MessageCallback, 0);
#endif

	// print GPU data
	std::cout << "GL_VENDOR: " << glGetString(GL_VENDOR) << std::endl;
	std::cout << "GL_RENDERER: " << glGetString(GL_RENDERER) << std::endl;
	std::cout << "GL_VERSION: " << glGetString(GL_VERSION) << std::endl;

	GLint totalMemKb = 0;
	glGetIntegerv(GL_GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX, &totalMemKb);
	std::cout << "GL_GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX: " << totalMemKb << std::endl;
	glGetIntegerv(GL_GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX, &totalMemKb);
	std::cout << "GL_GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX: " << totalMemKb << std::endl;

	std::cout << std::endl
		<< "Extensions: "
		<< std::endl;
	int numExtensions;
	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);
	for (int i = 0; i < numExtensions; i++)
	{
		std::cout << glGetStringi(GL_EXTENSIONS, i) << std::endl;
	}

	Scene this_scene;
	this_scene.init();


	SDL_Event event;
	while (!this_scene.quit)
	{
		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_QUIT:
				this_scene.quit = true;
				break;
			case SDL_KEYDOWN:
				this_scene.GetInput(event);
				break;
			}
		}

		this_scene.display(window);
	}

	this_scene.cleanup();

	SDL_GL_DeleteContext(context_);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}
