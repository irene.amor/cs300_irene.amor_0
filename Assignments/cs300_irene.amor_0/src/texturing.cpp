#include "texturing.h"

/**
* @brief Set a color into the array that makes up the texture
* 
* @param current:	the first position for the color
* @param color:		the color to add into the array
*
*/
void Texture::SetColor(unsigned char* current, unsigned char color[4])
{
	//Set the color channel by channel
	for(int i = 0; i < 4; i++)
		*(current+i) = color[i];
}

/**
* @brief Create a texture that looks like the one shown in the handout
*
*
*/
void Texture::CreateTexture()
{
	//Set the colors
	unsigned char purple[4]	{ 255, 0, 255, 255 };
	unsigned char blue[4]	{ 0, 0, 255, 255 };
	unsigned char cyan[4]	{ 0, 255, 255, 255 };
	unsigned char green[4]	{ 0, 255, 0, 255 };
	unsigned char yellow[4]	{ 255, 255, 0, 255 };
	unsigned char red[4]	{ 255, 0, 0, 255 };

	int pos = 0;

	//Set the texture
	//First row - from the bottom
	{
		SetColor(&texture[pos], purple); pos += 4;
		SetColor(&texture[pos], blue);	 pos += 4;
		SetColor(&texture[pos], cyan);	 pos += 4;
		SetColor(&texture[pos], green);	 pos += 4;
		SetColor(&texture[pos], yellow); pos += 4;
		SetColor(&texture[pos], red);	 pos += 4;
	}

	//Second row
	{
		SetColor(&texture[pos], red);	 pos += 4;
		SetColor(&texture[pos], purple); pos += 4;
		SetColor(&texture[pos], blue);	 pos += 4;
		SetColor(&texture[pos], cyan);	 pos += 4;
		SetColor(&texture[pos], green);	 pos += 4;
		SetColor(&texture[pos], yellow); pos += 4;
	}

	//Third row
	{
		SetColor(&texture[pos], yellow); pos += 4;
		SetColor(&texture[pos], red);	 pos += 4;
		SetColor(&texture[pos], purple); pos += 4;
		SetColor(&texture[pos], blue);	 pos += 4;
		SetColor(&texture[pos], cyan);	 pos += 4;
		SetColor(&texture[pos], green);	 pos += 4;
	}

	//Fourth row
	{
		SetColor(&texture[pos], green);	 pos += 4;
		SetColor(&texture[pos], yellow); pos += 4;
		SetColor(&texture[pos], red);	 pos += 4;
		SetColor(&texture[pos], purple); pos += 4;
		SetColor(&texture[pos], blue);	 pos += 4;
		SetColor(&texture[pos], cyan);	 pos += 4;
	}

	//Fifth row
	{
		SetColor(&texture[pos], cyan);	 pos += 4;
		SetColor(&texture[pos], green);	 pos += 4;
		SetColor(&texture[pos], yellow); pos += 4;
		SetColor(&texture[pos], red);	 pos += 4;
		SetColor(&texture[pos], purple); pos += 4;
		SetColor(&texture[pos], blue);	 pos += 4;
	}

	//Sixth row
	{
		SetColor(&texture[pos], blue);	 pos += 4;
		SetColor(&texture[pos], cyan);	 pos += 4;
		SetColor(&texture[pos], green);	 pos += 4;
		SetColor(&texture[pos], yellow); pos += 4;
		SetColor(&texture[pos], red);	 pos += 4;
		SetColor(&texture[pos], purple); pos += 4;
	}
}

/**
* @brief Load the texture with OpenGL tools
*
*
*/
void Texture::LoadTexture()
{
	CreateTexture();

	// Create texture
	glGenTextures(1, &texture_);
	glBindTexture(GL_TEXTURE_2D, texture_);

	// Give pixel data to opengl
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 6, 6, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glBindTexture(GL_TEXTURE_2D, 0);

}