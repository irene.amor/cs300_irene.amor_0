#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

#include <GL/glew.h>
#include <GL/GL.h>
#include <SDL2/SDL.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>



class Texture
{
public:
	void LoadTexture();
	
	GLuint texture_;

private:
	void CreateTexture();
	void SetColor(unsigned char* current, unsigned char color[4]);
	unsigned char texture[6*6*4];
};