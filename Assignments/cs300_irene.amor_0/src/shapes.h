#include <vector>

//-----------------------
//Shapes
//-----------------------
class Normal
{
public:

	float P0[4];
	float P1[4];
	float color[3]{ 1.f,1.f,1.f };

	float ave_normal[4];
	float face_normal[4];
};

class Vertex
{
public:
	float pos[4];
	float color[3];
	unsigned char UV[2];

	Normal normal;

	void CalculateFaceNormal(Vertex P1, Vertex P2);
	void CalculateAverageNormal(std::vector<Vertex> shape);
	void SetAllNormals(Vertex& P0, Vertex& P1, Vertex& P2);
};


class Shape
{
public:
	std::vector<Vertex> DrawShape(std::string mesh, unsigned change_subdiv);
	std::vector<Vertex> GetNormals(std::vector<Vertex> shape, bool average);

private:
	Vertex CreateVertex(float x, float y, float z);

	std::vector<Vertex> DrawFromMesh(std::string filename);

	void SetColors(std::vector<Vertex>& shape);

	void GetFaceNormals(std::vector<Vertex>& shape);
	void GetAverageNormals(std::vector<Vertex>& shape);

	void CylindricalCoords_UV(Vertex& vtx, float angle);
	void SphericalCoords_UV(Vertex& vtx, float angle_H, float angle_V);

	void CalculatePlane();
	void CalculateCube();

	std::vector<Vertex> DrawPlane();
	std::vector<Vertex> DrawCube();
	std::vector<Vertex> DrawCone(unsigned change_subdiv);
	std::vector<Vertex> DrawCylinder(unsigned change_subdiv);
	std::vector<Vertex> DrawSphere(unsigned change_subdiv);

	std::vector<Vertex> plane, cube;

	enum v { base_center, BR, BL, top_vtx, BR2, BL2 };

	enum { increase, decrease, keep };
	unsigned subdiv_cone = 4;
	unsigned subdiv_cylinder = 4;
	unsigned subdiv_sphere = 4;

	const float r = 0.5f;
	const float pi = glm::pi<float>();
};

