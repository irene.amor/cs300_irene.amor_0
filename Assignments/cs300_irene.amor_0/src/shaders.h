#include <string>

class Shaders
{
public:
	const std::string strVertexShader_Texture = R"(
	#version 400
	layout(location = 0) in vec4 aPosition;
	layout(location = 2) in vec2 UV;

	uniform mat4 MVP;

	out vec2 uv;
	void main()
	{
		gl_Position = MVP * aPosition;
		uv = UV/255;
	})";

	const std::string strFragmentShader_Texture = R"(
	#version 400
	in vec2 uv;

	uniform sampler2D textureData;

	out vec4 outputColor;
	void main()
	{
		vec4 color_ = texture(textureData, uv);
		outputColor = vec4(color_[0], color_[1], color_[2], 1.0f); 

	})";
		

	std::string strVertexShader_Color = R"(
	#version 400
	layout(location = 0) in vec4 aPosition;
	layout(location = 1) in vec3 aColor;

	uniform mat4 MVP;

	out vec3 color;
	void main()
	{
		gl_Position = MVP * aPosition;
		color = aColor;
	})";

	std::string strFragmentShader_Color = R"(
	#version 400
	in vec3 color;

	out vec4 outputColor;
	void main()
	{
		outputColor = vec4(color, 1.0f); 
	})";
};