/* ---------------------------------------------------------------------------------------------------------
Copyright (C) 2022 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
Project: cs300_irene.amor_0
Author: Irene Amor Méndez, irene.amor, 540001720
Creation date: 30th May, 2022
----------------------------------------------------------------------------------------------------------*/

- How to use the program: (same controls as specified in the handout)
	· Name the file to be read "input.txt"
	· Press W: 		make the vertical angle of the camera smaller
	· Press S: 		make the vertical angle of the camera greater
	· Press A: 		make the horizontal angle of the camera smaller
	· Press D: 		make the horizontal angle of the camera greater
	· Press Q: 		make the radius of the camera smaller
	· Press E: 		make the radius of the camera greater
	· Press N: 		toggle normal rendering
	· Press T: 		toggle texture mapping (on/off)
	· Press F: 		toggle face/averaged normals
	· Press M: 		toggle wireframe mode (on/off)
	· Press +/-: 	increase/reduce number of slices (4 is the minimum number of slices)


- Important parts of the code:
	· "scene.h" and "scene.cpp" contain the code needed to generate a scene. The necessary OpenGL functions
are called to draw the shapes and normals.
	· "shapes.h" and "shapes.cpp" contain the functions needed to generate the shapes: veretx generation, 
UV coordinates, colors, face-normals, averaged normals.
	· "texturing.h" and "texturing.cpp" create the texture.
	· "shaders.h" contain the vertex and fragment shaders for rendering with textures or with colors.


- Known issues and problems: (I haven't found any)