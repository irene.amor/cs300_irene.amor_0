/* Start Header -------------------------------------------------------
Copyright (C) 2011 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen
Institute of Technology is prohibited.
File Name: Mesh.cpp
Purpose: Definition of a mesh
Language: C++
Platform: Windows
Project: jon.sanchez_CS300_2
Author: Jon Sanchez
End Header --------------------------------------------------------*/
#include "MeshData.h"

#include <fstream>
#include <iomanip>
#include <iostream>
#include <unordered_map>

#define TINYOBJLOADER_IMPLEMENTATION
#include "tinyobj/tiny_obj_loader.h"
#include <glm/gtx/hash.hpp>

void MeshData::ClearAll()
{
    vertices.clear();
}

bool MeshData::LoadOBJ(const char * path)
{
    std::vector<unsigned int> vertexIndices, uvIndices, normalIndices;
    // OBJ data read
    std::vector<glm::vec2> temp_uvs;
    std::vector<glm::vec3> temp_normals;
    std::vector<glm::vec3> temp_vertices;

    // Every combination of vertices to give to VBO indexer
    std::vector<glm::vec2>           in_uvs;
    std::vector<glm::vec3>           in_normals;
    std::vector<glm::vec3>           in_vertices;
    tinyobj::attrib_t                attrib;
    std::vector<tinyobj::shape_t>    shapes;
    std::vector<tinyobj::material_t> materials;

    std::string warn;
    std::string err;
    bool        ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, path);
    if (!warn.empty())
    {
        std::cout << "WARNING LOADING MESH (" << path << "): " << warn << std::endl;
    }
    if (!err.empty())
    {
        std::cerr << "ERROR LOADING MESH (" << path << "): " << err << std::endl;
    }

    vertices.clear();
    for (size_t s = 0; s < shapes.size(); s++)
    {
        const tinyobj::mesh_t & mesh = shapes[s].mesh;
        for (size_t i = 0; i < mesh.indices.size(); i++)
        {
            const tinyobj::index_t & index = mesh.indices[i];

            Vertex v;

            v.pos = glm::vec3 {attrib.vertices[index.vertex_index * 3 + 0],
                               attrib.vertices[index.vertex_index * 3 + 1],
                               attrib.vertices[index.vertex_index * 3 + 2]};

            v.normal = glm::vec3 {attrib.normals[index.normal_index * 3 + 0],
                                  attrib.normals[index.normal_index * 3 + 1],
                                  attrib.normals[index.normal_index * 3 + 2]};

            v.uv = glm::vec2 {attrib.texcoords[index.texcoord_index * 2 + 0],
                              attrib.texcoords[index.texcoord_index * 2 + 1]};

            vertices.push_back(v);
        }
    }

    ComputeTangentBasis();

    return true;
}

void MeshData::ExportOBJ(const char * path, bool exportTangent)
{
    std::ofstream outFile(path);

    if (!outFile.is_open())
        return;

    outFile << std::fixed << std::setprecision(6);

    std::unordered_map<glm::vec3, unsigned short> exportedVertices;
    unsigned short                                vertIndex = 0;

    for (unsigned i = 0; i < vertices.size(); i++)
    {
        // Check if we already exported this vertex
        std::unordered_map<glm::vec3, unsigned short>::iterator found = exportedVertices.find(vertices[i].pos);
        // Export if we didnt
        if (found == exportedVertices.end())
        {
            outFile << "v ";
            outFile << vertices[i].pos.x << " ";
            outFile << vertices[i].pos.y << " ";
            outFile << vertices[i].pos.z << std::endl;

            exportedVertices.insert(std::pair<glm::vec3, unsigned short>(vertices[i].pos, vertIndex++));
        }
    }

    std::unordered_map<glm::vec2, unsigned short> exportedUVs;
    unsigned short                                uvIndex = 0;

    for (unsigned i = 0; i < vertices.size(); i++)
    {
        // Check if we already exported this uv
        std::unordered_map<glm::vec2, unsigned short>::iterator found = exportedUVs.find(vertices[i].uv);
        // Export if we didnt
        if (found == exportedUVs.end())
        {
            outFile << "vt ";
            outFile << vertices[i].uv.x << " ";
            outFile << vertices[i].uv.y << std::endl;

            exportedUVs.insert(std::pair<glm::vec2, unsigned short>(vertices[i].uv, uvIndex++));
        }
    }

    std::unordered_map<glm::vec3, unsigned short> exportedNormals;
    unsigned short                                normalIndex = 0;

    for (unsigned i = 0; i < vertices.size(); i++)
    {
        // Check if we already exported this normal
        std::unordered_map<glm::vec3, unsigned short>::iterator found = exportedNormals.find(vertices[i].normal);
        // Export if we didnt
        if (found == exportedNormals.end())
        {
            outFile << "vn ";
            outFile << vertices[i].normal.x << " ";
            outFile << vertices[i].normal.y << " ";
            outFile << vertices[i].normal.z << std::endl;

            exportedNormals.insert(std::pair<glm::vec3, unsigned short>(vertices[i].normal, normalIndex++));
        }
    }

    std::unordered_map<glm::vec3, unsigned short> exportedTangents;
    std::unordered_map<glm::vec3, unsigned short> exportedBitangents;

    if (exportTangent)
    {
        unsigned short tangentIndex = 0;

        for (unsigned i = 0; i < vertices.size(); i++)
        {
            // Check if we already exported this normal
            std::unordered_map<glm::vec3, unsigned short>::iterator found = exportedTangents.find(vertices[i].tangent);
            // Export if we didnt
            if (found == exportedTangents.end())
            {
                outFile << "vta ";
                outFile << vertices[i].tangent.x << " ";
                outFile << vertices[i].tangent.y << " ";
                outFile << vertices[i].tangent.z << std::endl;

                exportedTangents.insert(std::pair<glm::vec3, unsigned short>(vertices[i].tangent, tangentIndex++));
            }
        }
        unsigned short bitangentIndex = 0;

        for (unsigned i = 0; i < vertices.size(); i++)
        {
            // Check if we already exported this normal
            std::unordered_map<glm::vec3, unsigned short>::iterator found = exportedBitangents.find(vertices[i].bitangent);
            // Export if we didnt
            if (found == exportedBitangents.end())
            {
                outFile << "vbi ";
                outFile << vertices[i].bitangent.x << " ";
                outFile << vertices[i].bitangent.y << " ";
                outFile << vertices[i].bitangent.z << std::endl;

                exportedBitangents.insert(std::pair<glm::vec3, unsigned short>(vertices[i].bitangent, bitangentIndex++));
            }
        }
    }

    // Export all faces
    for (unsigned i = 0; i < vertices.size(); i += 3)
    {
        Vertex & v0 = vertices[i + 0];
        Vertex & v1 = vertices[i + 1];
        Vertex & v2 = vertices[i + 2];


        outFile << "f ";

        outFile << (exportedVertices.find(v0.pos)->second + 1) << "/";
        outFile << (exportedUVs.find(v0.uv)->second + 1) << "/";
        outFile << (exportedNormals.find(v0.normal)->second + 1);
        if (exportTangent)
        {
            outFile << "/" << (exportedTangents.find(v0.tangent)->second + 1);
            outFile << "/" << (exportedBitangents.find(v0.bitangent)->second + 1);
        }

        outFile << " ";

        outFile << (exportedVertices.find(v1.pos)->second + 1) << "/";
        outFile << (exportedUVs.find(v1.uv)->second + 1) << "/";
        outFile << (exportedNormals.find(v1.normal)->second + 1);
        if (exportTangent)
        {
            outFile << "/" << (exportedTangents.find(v1.tangent)->second + 1);
            outFile << "/" << (exportedBitangents.find(v1.bitangent)->second + 1);
        }

        outFile << " ";

        outFile << (exportedVertices.find(v2.pos)->second + 1) << "/";
        outFile << (exportedUVs.find(v2.uv)->second + 1) << "/";
        outFile << (exportedNormals.find(v2.normal)->second + 1);
        if (exportTangent)
        {
            outFile << "/" << (exportedTangents.find(v2.tangent)->second + 1);
            outFile << "/" << (exportedBitangents.find(v2.bitangent)->second + 1);
        }

        outFile << std::endl;
    }
}

//////// STUDENT CODE
void MeshData::ComputeTangentBasis()
{
    // Reset tangents and bitangents to zero vectors
    for (int i = 0; i < vertices.size(); i++)
    {
        vertices[i].tangent = { 0.f, 0.f, 0.f };
        vertices[i].bitangent = { 0.f, 0.f, 0.f };
    }

    // Loop through the triangles (using the index buffer)
    for (int i = 0; i < vertices.size(); i+=3)
    {
        // Solve equations to find T and B for this triangle
        glm::vec3 V1 = vertices[i + 1].pos - vertices[i + 0].pos;
        glm::vec3 V2 = vertices[i + 2].pos - vertices[i + 0].pos;

        glm::vec2 uv1 = vertices[i + 1].uv - vertices[i + 0].uv;
        glm::vec2 uv2 = vertices[i + 2].uv - vertices[i + 0].uv;

        float denom = (uv1.y * uv2.x - uv2.y * uv1.x);
        glm::vec3 T = (uv1.y * V2 - uv2.y * V1) / denom;
        glm::vec3 B = (uv2.x * V1 - uv1.x * V2) / denom;

        if (denom == 0.f)
        {
            T = { 1.f, 0.f, 0.f };
            B = { 0.f, 1.f, 0.f };
        }

        // Accumulate tangent/bitangent for the 3 vertices of the triangle (to average after)
        for (int j = 0; j < 3; j++)
        {
            vertices[i + j].tangent = T;
            vertices[i + j].bitangent = B;
        }
    }

        
    // Loop through every vertex
    for (int i = 0; i < vertices.size(); i++)
    {
        // Set normal to (0,0,1) when lenght is 0
        if (glm::length(vertices[i].normal) == 0)
            vertices[i].normal = { 0.f,0.f,1.f };

        //Check perpendicularity
        //Only the tangent is perpendicular
        if (dot(vertices[i].tangent, vertices[i].normal) == 0 
            && dot(vertices[i].bitangent, vertices[i].normal) != 0)
        {
            glm::vec3 newBitangent = glm::cross(vertices[i].normal, vertices[i].tangent);
            
            if (dot(vertices[i].bitangent, newBitangent) < 0)
                vertices[i].bitangent = -newBitangent;
            else
                vertices[i].bitangent = newBitangent;
        }

        //Only the bitangent is perpendicular
        else if (dot(vertices[i].tangent, vertices[i].normal) != 0 
            && dot(vertices[i].bitangent, vertices[i].normal) == 0)
        {
            glm::vec3 newTangent = glm::cross(vertices[i].normal, vertices[i].bitangent);

            if (dot(vertices[i].tangent, newTangent) < 0)
                vertices[i].tangent = -newTangent;
            else
                vertices[i].tangent = newTangent;
        }

        //Neither are perpendicular
        else if (dot(vertices[i].tangent, vertices[i].normal) != 0
            && dot(vertices[i].bitangent, vertices[i].normal) != 0)
        {

            // Gram-Schmidt orthogonalization of tangent respect to normal and normalize tangent
            glm::vec3 projNT = static_cast<float>((dot(vertices[i].tangent, vertices[i].normal) / pow(glm::length(vertices[i].normal), 2))) * vertices[i].normal;

            glm::vec3 newTangent = vertices[i].tangent - projNT;

            // Compute the new perpendicular bitangent maintaining the original handeness of the previously
            glm::vec3 newBitangent = glm::cross(vertices[i].normal, newTangent);


            if(dot(vertices[i].tangent, newTangent) < 0)
                vertices[i].tangent = -newTangent;
            else
                vertices[i].tangent = newTangent;

            if (dot(vertices[i].bitangent, newBitangent) < 0)
                vertices[i].bitangent = -newBitangent;
            else
                vertices[i].bitangent = newBitangent;

            

            // computed one (T,B,N need to be normalized and orthogonal at this point)
        }

        vertices[i].normal = normalize(vertices[i].normal);
        vertices[i].tangent = normalize(vertices[i].tangent);
        vertices[i].bitangent = normalize(vertices[i].bitangent);
    }
}
