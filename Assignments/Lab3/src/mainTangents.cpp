
#include "MeshData.h"

int main(void)
{
	MeshData mesh;

	mesh.LoadOBJ("data/meshes/plane.obj");
	mesh.ExportOBJ("data/meshes/plane_tangents.obj");
	
	mesh.LoadOBJ("data/meshes/cube_averaged.obj");
	mesh.ExportOBJ("data/meshes/cube_tangents.obj");

	mesh.LoadOBJ("data/meshes/cone_20_averaged.obj");
	mesh.ExportOBJ("data/meshes/cone_20_tangents.obj");

	mesh.LoadOBJ("data/meshes/cylinder_20_averaged.obj");
	mesh.ExportOBJ("data/meshes/cylinder_20_tangents.obj");

	mesh.LoadOBJ("data/meshes/sphere_20_averaged.obj");
	mesh.ExportOBJ("data/meshes/sphere_20_tangents.obj");

	return 0;
}