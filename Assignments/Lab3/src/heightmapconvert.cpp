#include <iostream>

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb/stb_image_write.h>

#include <glm/glm.hpp>


void CreateNormalMap(const std::string & heightMap, const std::string & normalMap, float scaleFactor)
{
    // Load heightMap
    int       w             = 0;
    int       h             = 0;
    int       format        = 0;
    stbi_uc * loadedSurface = stbi_load(heightMap.c_str(), &w, &h, &format, 4);

    // Interpret pixels as byte per color channel
    unsigned char * heightPixels = static_cast<unsigned char *>(loadedSurface);
    unsigned char * normalPixels = new unsigned char[w * h * 4];

    // Init the new texture data to 0
    std::memset(normalPixels, 0, w * h * 4);

    // edge pixels will be ignored by the loop and they will be left black
    for (int x = 1; x < w - 1; x++)
    {
        for (int y = 1; y < h - 1; y++)
        {
            const unsigned index = y * w * 4 + x * 4;

            /////////// STUDENT CODE //////////
            //Get the indexes for each pixel
            const unsigned index_L = y * w * 4 + (x-1) * 4;
            const unsigned index_R = y * w * 4 + (x+1) * 4;
            //We are going down on the texture
            const unsigned index_B = (y+1) * w * 4 + x * 4;
            const unsigned index_T = (y-1) * w * 4 + x * 4;

            //Find the two tangents at any pixel
            glm::vec3 S = { 1.f, 0.f, scaleFactor * ((heightPixels[index_R]/255.f) - (heightPixels[index_L]/255.f)) };
            glm::vec3 T = { 0.f, 1.f, scaleFactor * ((heightPixels[index_T]/255.f) - (heightPixels[index_B]/255.f)) };
           
            //Calculate the normal
            glm::vec3 N = glm::cross(S, T) / glm::length(glm::cross(S, T));

            //Set the color
            normalPixels[index + 0] = floor(255.99f * 0.5f * (N[0] + 1));
            normalPixels[index + 1] = floor(255.99f * 0.5f * (N[1] + 1));
            normalPixels[index + 2] = floor(255.99f * 0.5f * (N[2] + 1));
            normalPixels[index + 3] = 255;

            /////////////////////////////////////
        }
    }

    // Store output image from the generated data
    stbi_write_png(normalMap.c_str(), w, h, 4, normalPixels, w * 4);

    delete[] normalPixels;
}


int main(int argc, char * args[])
{
    CreateNormalMap("data/textures/brick_height.png", "data/textures/brick_height_n_out.png", 10.0f);
    CreateNormalMap("data/textures/brick2_height.png", "data/textures/brick2_height_n_out.png", 3.0f);
    CreateNormalMap("data/textures/voronoi_height.png", "data/textures/voronoi_height_n_out.png", 50.0f);

    return 0;
}