
/* Start Header -------------------------------------------------------
Copyright (C) 2011 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen
Institute of Technology is prohibited.
File Name: Mesh.h
Purpose: Declaration of a Mesh class
Language: C++
Platform: Windows
Project: jon.sanchez_CS300_2
Author: Jon Sanchez
End Header --------------------------------------------------------*/
#pragma once

#include <map>
#include <string>
#include <vector>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform2.hpp>


class MeshData
{
  public:
    struct Vertex
    {
        glm::vec3 pos;
        glm::vec2 uv;
        glm::vec3 normal;
        glm::vec3 tangent;
        glm::vec3 bitangent;
    };

    // Loads the OBJ file in the mesh
    bool LoadOBJ(const char * path);

    void ExportOBJ(const char * path, bool exportTangent = true);

    // Compute the tangent and bitangent per vertex
    void ComputeTangentBasis();

    // Clear all data of mesh
    void ClearAll();

    std::vector<Vertex> vertices;
};
