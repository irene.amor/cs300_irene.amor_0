#version 400
layout(location = 0) in vec4 aPosition;
layout(location = 1) in vec3 aColor;
layout(location = 2) in vec2 UV;
layout(location = 3) in float ambient;
layout(location = 4) in vec3  diffuse_color;
layout(location = 6) in float shininess;
layout(location = 7) in vec4  face_normal;
layout(location = 8) in float  is_white;

uniform mat4 MVP;
uniform mat4 M2W;
uniform mat4 viewMtx;

uniform float using_tex;

out float white;
out vec2 uv;
out vec3 color;
out vec4 fragPos;
out vec4 faceNormal;
out float ambientObj;
out float shininessObj;

void main()
{		
	gl_Position	= MVP * aPosition;
		
	white = is_white;
	color = aColor;
	uv			= UV/255;
	fragPos		= (viewMtx * M2W * vec4(aPosition.xyz, 1.f));				//Vertex position in world space

	faceNormal	= (transpose(inverse(viewMtx * M2W)) * (face_normal));	//Vertex normal in world space
	faceNormal.w = 0.f;

	ambientObj		= ambient;
	shininessObj	= shininess;
}