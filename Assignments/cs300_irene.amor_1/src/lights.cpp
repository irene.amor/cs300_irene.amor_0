#include "lights.h"


/**
* @brief Initializes the parameters to their default values
*
*
*/
MaterialParameters::MaterialParameters()
{
	for (int i = 0; i < 3; i++)
	{
		attenuation_color[i] = 1.f;
		specular_color[i] = 1.f;
		diffuse_color[i] = 1.f;
	}

	ambient = 1.f;
	shininess = 10.f;
}


/**
* @brief Gets the shape for a light
*
* @return: the vertices that form the sphere used for the light
* 
*/
std::vector<Vertex> Lights::GetLight()
{
	if (lights_sphere.size() == 0)
	{
		Shape s;
		lights_sphere = s.DrawLightSphere();
	}

	return lights_sphere;
}

