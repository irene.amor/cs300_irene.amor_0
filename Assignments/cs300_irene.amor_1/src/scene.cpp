#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

#include <GL/glew.h>
#include <GL/GL.h>
#include <SDL2/SDL.h>

#include "OGLDebug.h"
#include "scene.h"
#include "CS300Parser.h"
#include "ShaderUtils.h"


static int     winID;
static GLsizei WIDTH = 1280;
static GLsizei HEIGHT = 720;



void Scene::InitializeProgram()
{
	if (theProgram_Tex != 0)
		glDeleteProgram(theProgram_Tex);

	theProgram_Tex = ShaderUtils::CreateShaderProgram("data/shaders/vertex.vert", "data/shaders/fragment.frag");

	//Create the arrays of VAOs and VBOs
	AO_S.clear();
	AO_N.clear();
	for (unsigned i = 0; i < parser.objects.size(); i++)
	{
		AO_S.push_back(*new ArraysObject);
		AO_N.push_back(*new ArraysObject);
	}
	for (unsigned i = 0; i < parser.lights.size(); i++)
		AO_N.push_back(*new ArraysObject);


	//Enable all the culling, backface removal...
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glEnable(GL_DEPTH_TEST);
	glFrontFace(GL_CCW);
	glDepthFunc(GL_LESS);
	glDepthRange(0.f, 1.f);

}

void Scene::InitializeBuffers()
{
	BufferCleanup();

	size_t max_obj = parser.objects.size();
	size_t max_lights = parser.lights.size();


	size_t max = (max_obj > max_lights) ? max_obj : max_lights;
	for (unsigned i = 0; i < max; i++)
	{

		for (unsigned j = 0; j < 2; j++)
		{
			ArraysObject* AO = nullptr;
			std::vector<Vertex> vtx;

			if ((j == 0 && i < max_lights) || j == 1)
			{
				if (j == 0 && i < max_lights)
				{
					size_t idx = i + max_obj;
					AO = &AO_N[idx];
					vtx = lights[i].GetLight();
					vtx_count[idx] = vtx.size();

					//Set to draw as white
					for (int k = 0; k < vtx.size(); k++)
						vtx[k].is_white = 1.f;	//true
				}
				else if(j == 1)
				{
					AO = &AO_S[i];
					const char* mesh_name = parser.objects[i].mesh.c_str();
					vtx = shape.DrawShape(parser.objects[i].mesh, change_subdiv, parser.objects[i].ns);
					vtx_count[i] = vtx.size();

					// VAO - NORMALS
					if (draw_normals)
					{
						std::vector<Vertex> normals;
						//Average normals
						if (average_normals)
							normals = shape.GetNormals(vtx, true);
						//Face normals
						else
							normals = shape.GetNormals(vtx, false);

						//Set to draw as white
						for (int k = 0; k < normals.size(); k++)
							normals[k].is_white = 1.f;	//true

						normals_count[i] = 0;
						normals_count[i] = normals.size();

						glGenVertexArrays(1, &AO_N[i].vao);
						glBindVertexArray(AO_N[i].vao);



						// VBO for positions
						glGenBuffers(1, &AO_N[i].BufferObjects[0]);

						glBindBuffer(GL_ARRAY_BUFFER, AO_N[i].BufferObjects[0]);
						glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * normals.size(), normals.data(), GL_STATIC_DRAW);

						// Insert the VBO into the VAO
						glEnableVertexAttribArray(0);
						glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, pos)));


						// VBO for colors
						glGenBuffers(1, &AO_N[i].BufferObjects[1]);

						glBindBuffer(GL_ARRAY_BUFFER, AO_N[i].BufferObjects[1]);
						glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * normals.size(), normals.data(), GL_STATIC_DRAW);

						glEnableVertexAttribArray(1);
						glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, color)));
						


						// VBO for colors
						glGenBuffers(1, &AO_N[i].BufferObjects[8]);

						glBindBuffer(GL_ARRAY_BUFFER, AO_N[i].BufferObjects[8]);
						glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * normals.size(), normals.data(), GL_STATIC_DRAW);

						glEnableVertexAttribArray(8);
						glVertexAttribPointer(8, 1, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, is_white)));

					}
				}




				// VAO - SHAPES
				{
					glGenVertexArrays(1, &AO->vao);
					glBindVertexArray(AO->vao);


					// VBO for positions
					glGenBuffers(1, &AO->BufferObjects[0]);

 					glBindBuffer(GL_ARRAY_BUFFER, AO->BufferObjects[0]);
					glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vtx.size(), vtx.data(), GL_STATIC_DRAW);

					// Insert the VBO into the VAO
					glEnableVertexAttribArray(0);
					glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);


					// VBO for colors
					glGenBuffers(1, &AO->BufferObjects[1]);

					glBindBuffer(GL_ARRAY_BUFFER, AO->BufferObjects[1]);
					glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vtx.size(), vtx.data(), GL_STATIC_DRAW);

					glEnableVertexAttribArray(1);
					glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, color)));


					// VBO for UVs
					glGenBuffers(1, &AO->BufferObjects[2]);

					glBindBuffer(GL_ARRAY_BUFFER, AO->BufferObjects[2]);
					glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vtx.size(), vtx.data(), GL_STATIC_DRAW);

					// Insert the VBO into the VAO
					glEnableVertexAttribArray(2);
					glVertexAttribPointer(2, 2, GL_UNSIGNED_BYTE, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, UV)));

				}


				// VAO - SHAPES' MATERIAL PROPERTIES
				{
					// VBO for ambient
					glGenBuffers(1, &AO->BufferObjects[3]);

					glBindBuffer(GL_ARRAY_BUFFER, AO->BufferObjects[3]);
					glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vtx.size(), vtx.data(), GL_STATIC_DRAW);

					// Insert the VBO into the VAO
					glEnableVertexAttribArray(3);
					glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, ambient)));



					// VBO for diffuse
					glGenBuffers(1, &AO->BufferObjects[4]);

					glBindBuffer(GL_ARRAY_BUFFER, AO->BufferObjects[4]);
					glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vtx.size(), vtx.data(), GL_STATIC_DRAW);

					// Insert the VBO into the VAO
					glEnableVertexAttribArray(4);
					glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, diffuse_color)));



					// VBO for specular
					glGenBuffers(1, &AO->BufferObjects[5]);

					glBindBuffer(GL_ARRAY_BUFFER, AO->BufferObjects[5]);
					glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vtx.size(), vtx.data(), GL_STATIC_DRAW);

					// Insert the VBO into the VAO
					glEnableVertexAttribArray(5);
					glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, specular_color)));



					// VBO for shininess
					glGenBuffers(1, &AO->BufferObjects[6]);

					glBindBuffer(GL_ARRAY_BUFFER, AO->BufferObjects[6]);
					glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vtx.size(), vtx.data(), GL_STATIC_DRAW);

					// Insert the VBO into the VAO
					glEnableVertexAttribArray(6);
					glVertexAttribPointer(6, 1, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, shininess)));



					// VBO for face normal 
					glGenBuffers(1, &AO->BufferObjects[7]);

					glBindBuffer(GL_ARRAY_BUFFER, AO->BufferObjects[7]);
					glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vtx.size(), vtx.data(), GL_STATIC_DRAW);

					// Insert the VBO into the VAO
					glEnableVertexAttribArray(7);

					if(average_normals)
						glVertexAttribPointer(7, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, ave_normal)));
					else
						glVertexAttribPointer(7, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, face_normal)));




					// VBO for colors
					glGenBuffers(1, &AO_N[i].BufferObjects[8]);

					glBindBuffer(GL_ARRAY_BUFFER, AO_N[i].BufferObjects[8]);
					glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex)* vtx.size(), vtx.data(), GL_STATIC_DRAW);

					glEnableVertexAttribArray(8);
					glVertexAttribPointer(8, 1, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, is_white)));

				}

			}
		}

	}
	
}


/**
* @brief Updates the camera view matrix after getting input from the user
*
* @return: the new view matrix
*/
glm::mat4 Scene::UpdateCameraViewMtx()
{
	//Get the position of the camera (will need to be offsetted by the target
	glm::vec3 pos{	cam_radius * sinf(alpha) * sinf(beta),
					cam_radius * cosf(alpha),
					cam_radius * sinf(alpha) * cosf(beta)};

	//Get the view matrix
	glm::mat4 View = glm::lookAt(-pos + parser.camTarget, parser.camTarget, parser.camUp);

	return View;

}

/**
* @brief Create the transformation matrices for each object
*
*/
void Scene::TransfMatrices(unsigned idx)
{
	size_t TOTAL_obj = parser.objects.size();
	if (idx < TOTAL_obj)
	{
		CS300Parser::Transform transf = parser.objects[idx];

		//Play animations to get the current position
		glm::vec3 currentPos = transf.pos;

		for (size_t u = 0; u < transf.anims.size(); u++)
		{
			currentPos = transf.anims[u].Update(transf.pos, SDL_GetTicks()/1000.f);
		}

		//Create the matrices for the transformation with data from the parser
		glm::mat4 T = glm::translate(glm::mat4(1.f), currentPos);
		glm::mat4 Rx = glm::rotate(glm::mat4(1.f), glm::radians(transf.rot.x), glm::vec3(1.f, 0.f, 0.f));
		glm::mat4 Ry = glm::rotate(glm::mat4(1.f), glm::radians(transf.rot.y), glm::vec3(0.f, 1.f, 0.f));
		glm::mat4 Rz = glm::rotate(glm::mat4(1.f), glm::radians(transf.rot.z), glm::vec3(0.f, 0.f, 1.f));
		glm::mat4 R = Rz * Ry * Rx;
		glm::mat4 S = glm::scale(glm::mat4(1.f), glm::vec3(transf.sca.x, transf.sca.y, transf.sca.z));

		//Create the transformation matrix of the current object
		glm::mat4 transf_mtx = T * R * S;
		transform[idx] = transf_mtx;
	}

	size_t TOTAL_lights = parser.lights.size();
	if (idx >= TOTAL_obj && idx < TOTAL_lights + TOTAL_obj)
	{
		CS300Parser::Light light = parser.lights[idx - TOTAL_obj];

		//Play animations
		glm::vec3 currentPos = light.pos;

		for (unsigned u = 0; u < light.anims.size(); u++)
		{
			currentPos = light.anims[u].Update(currentPos, SDL_GetTicks() / 1000.f);
		}

		for (unsigned j = 0; j < 3; j++)
			lights[idx - TOTAL_obj].currentPos[j] = currentPos[j];



		//Create the matrices for the transformation with data from the parser
		glm::mat4 T = glm::translate(glm::mat4(1.f), currentPos);
		glm::mat4 S = glm::scale(glm::mat4(1.f), glm::vec3(2.f, 2.f, 2.f));

		//Create the transformation matrix of the current object
		glm::mat4 transf_mtx = T * S;
		transform[idx] = transf_mtx;
	}
}

/**
* @brief Get keyboard input from the user
*
*/
void Scene::GetInput(SDL_Event event)
{
	//Quit the program
	if (event.key.keysym.scancode == SDL_SCANCODE_ESCAPE)
		quit = true;

	//Change the number of subdivisions of the shapess
	else if (event.key.keysym.scancode == SDL_SCANCODE_KP_PLUS)
	{
		change_subdiv = increase;
		update();
	}
	else if (event.key.keysym.scancode == SDL_SCANCODE_KP_MINUS)
	{
		change_subdiv = decrease;
		update();
	}

	//Change the camera's vertical angle
	else if (event.key.keysym.scancode == SDL_SCANCODE_W)
	{
		if (alpha > 0.06f && alpha < 3.1f)
			alpha -= 0.05f;
	}
	else if (event.key.keysym.scancode == SDL_SCANCODE_S)
	{
		if (alpha >= 0.f && alpha < 3.06f)
			alpha += 0.05f;
	}

	//Change the camera's horizontal angle
	else if (event.key.keysym.scancode == SDL_SCANCODE_A)
	{
		beta -= 0.05f;
	}
	else if (event.key.keysym.scancode == SDL_SCANCODE_D)
	{
		beta += 0.05f;
	}

	//Change the camera's radius
	else if (event.key.keysym.scancode == SDL_SCANCODE_E)
	{
		if (abs(cam_radius) > 0 && abs(cam_radius) <= abs(max_cam_radius))
			cam_radius -= 1.f;
	}
	else if (event.key.keysym.scancode == SDL_SCANCODE_Q)
	{
		if (abs(cam_radius) > 0 && abs(cam_radius) <= abs(max_cam_radius))
			cam_radius += 1.f;
	}

	//Change to using texture or color
	else if (event.key.keysym.scancode == SDL_SCANCODE_T)
	{
		using_texture = !using_texture;
		update();
	}

	//See shapes in wireframe or full mode
	else if (event.key.keysym.scancode == SDL_SCANCODE_M)
	{
		wireframe_mode = !wireframe_mode;
	}

	//Draw the normals
	else if (event.key.keysym.scancode == SDL_SCANCODE_N)
	{
		draw_normals = !draw_normals;
		update();
	}

	//Change from average normals to face normals
	else if (event.key.keysym.scancode == SDL_SCANCODE_F)
	{
		average_normals = !average_normals;
		update();
	}
}



void Scene::InitializeLights()
{
	for (unsigned i = 0; i < parser.lights.size(); i++)
	{
		lights.push_back(*new Lights);
		lights[i].material_p.ambient = parser.lights[i].amb;

		//Set the attenuation, specular and light colors
		for (unsigned j = 0; j < 3; j++)
		{
			lights[i].material_p.attenuation_color[j] = parser.lights[i].att[j];
			lights[i].material_p.specular_color[j] = 1.f;		//White
			lights[i].lightColor[j] = parser.lights[i].col[j];
		}

		//Set Type of light
		if (!strcmp(parser.lights[i].type.c_str(), "POINT"))
			lights[i].type = POINT;
		else if (!strcmp(parser.lights[i].type.c_str(), "DIR"))
			lights[i].type = DIR;
		else if (!strcmp(parser.lights[i].type.c_str(), "SPOT"))
		{
			//Get the angles and cutoff value
			lights[i].type = SPOT;
			lights[i].spotExponent = parser.lights[i].falloff;
			lights[i].spotCosInner = cosf(glm::radians(parser.lights[i].inner));
			lights[i].spotCosOuter = cosf(glm::radians(parser.lights[i].outer));
		}
		
		//Set the direction of the light
		if(lights[i].type == DIR || lights[i].type == SPOT)
		{
			for (unsigned j = 0; j < 3; j++)
				lights[i].spotDirection[j] = parser.lights[i].dir[j];
		}

	}
}

void Scene::InitializeUniforms()
{
	//Set the texture generated procedurally
	texture.LoadTexture();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture.texture_);

	GLuint loc_tex = glGetUniformLocation(theProgram_Tex, "textureData");
	glUniform1i(loc_tex, 0);


	//Set light parameters
	{
		const int MAX_LIGHTS = 8;

		float l_ambient = parser.lights[0].amb;
		int l_type[MAX_LIGHTS] = { -1, -1, -1, -1, -1, -1, -1, -1 };

		float l_falloff[MAX_LIGHTS] = { 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f };
		float l_cos_inner[MAX_LIGHTS] = { 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f };
		float l_cos_outer[MAX_LIGHTS] = { 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f };

		glm::vec3 l_color[MAX_LIGHTS]{ glm::vec3(0.f, 0.f, 0.f) };
		glm::vec3 l_direction[MAX_LIGHTS]{ glm::vec3(0.f, 0.f, 0.f) };
		glm::vec3 l_diffuse[MAX_LIGHTS]{ glm::vec3(0.f, 0.f, 0.f) };
		glm::vec3 l_specular[MAX_LIGHTS]{ glm::vec3(0.f, 0.f, 0.f) };
		glm::vec3 l_attenuation[MAX_LIGHTS]{ glm::vec3(0.f, 0.f, 0.f) };

		int max = static_cast<int>(parser.lights.size());

		//Put the values into vectors
		for (int i = 0; i < max; i++)
		{
			for (unsigned j = 0; j < 3; j++)
			{
				l_color[i][j] = lights[i].lightColor[j];
				l_diffuse[i][j] = lights[i].material_p.diffuse_color[j];
				l_specular[i][j] = lights[i].material_p.specular_color[j];

				//FOR POINT AND SPOTLIGHT
				l_attenuation[i][j] = lights[i].material_p.attenuation_color[j];

				//FOR SPOTLIGHT
				if (lights[i].type == DIR || lights[i].type == SPOT)
					l_direction[i][j] = lights[i].spotDirection[j];

			}

			if (lights[i].type == SPOT)
			{
				l_falloff[i] = lights[i].spotExponent;
				l_cos_inner[i] = lights[i].spotCosInner;
				l_cos_outer[i] = lights[i].spotCosOuter;
			}

			l_type[i] = lights[i].type;
		}


		//Set the uniform vectors
		for (int i = 0; i < 1; i++)
		{
			glUseProgram(theProgram_Tex);

			//Lights count
			GLuint loc_count = glGetUniformLocation(theProgram_Tex, "max_lights");
			glUniform1i(loc_count, max);

			//Light type
			GLuint loc_type = glGetUniformLocation(theProgram_Tex, "l_type");
			glUniform1iv(loc_type, max, &l_type[0]);

			//Lght color
			GLuint loc_color = glGetUniformLocation(theProgram_Tex, "l_color");
			glUniform3fv(loc_color, max, &l_color[0][0]);

			//Ambient
			GLuint loc_amb = glGetUniformLocation(theProgram_Tex, "l_ambient");
			glUniform1f(loc_amb, l_ambient);

			//Diffuse
			GLuint loc_diff = glGetUniformLocation(theProgram_Tex, "l_diffuse");
			glUniform3fv(loc_diff, max, &l_diffuse[0][0]);

			//Specular
			GLuint loc_spec = glGetUniformLocation(theProgram_Tex, "l_specular");
			glUniform3fv(loc_spec, max, &l_specular[0][0]);

			//Attenuation
			GLuint loc_att = glGetUniformLocation(theProgram_Tex, "l_attenuation");
			glUniform3fv(loc_att, max, &l_attenuation[0][0]);


			//DIRECTIONAL LIGHTS AND SPOTLIGHTS
			//Direction
			GLuint loc_dir = glGetUniformLocation(theProgram_Tex, "l_direction");
			glUniform3fv(loc_dir, max, &l_direction[0][0]);

			//SPOTLIGHTS
			//Fallof value
			GLuint loc_fall = glGetUniformLocation(theProgram_Tex, "l_falloff");
			glUniform1fv(loc_fall, max, &l_falloff[0]);

			//Cosine of the inner angle value
			GLuint loc_inner = glGetUniformLocation(theProgram_Tex, "l_cos_inner");
			glUniform1fv(loc_inner, max, &l_cos_inner[0]);

			//Cosine of the outer angle value
			GLuint loc_outer = glGetUniformLocation(theProgram_Tex, "l_cos_outer");
			glUniform1fv(loc_outer, max, &l_cos_outer[0]);
		}

	}

}

void Scene::SetLightPosUniforms()
{
	const int MAX_LIGHTS = 8;
	glm::vec3 l_position[MAX_LIGHTS]{ glm::vec3(0.f, 0.f, 0.f) };
	
	unsigned max = static_cast<unsigned>(parser.lights.size());
	
	//Put the positions into vectors
	for (unsigned i = 0; i < max; i++)
	{
		glm::vec3 cPos = glm::vec4(lights[i].currentPos[0], lights[i].currentPos[1], lights[i].currentPos[2], 1.f);
		for (unsigned j = 0; j < 3; j++)
		{
			l_position[i][j] = cPos[j];
		}
	}

	//Set the uniform for the positions of the lights
	GLuint loc_pos = glGetUniformLocation(theProgram_Tex, "l_position");
	glUniform3fv(loc_pos, max, &l_position[0][0]);

}



//Called after the window and OpenGL are initialized. Called exactly once, before the main loop.
void Scene::init()
{
	//Load the parser
	parser.LoadDataFromFile("input.txt");

	//Camera info
	cam_radius = parser.camTarget.z - parser.camPos.z;
	max_cam_radius = cam_radius * 2.5f;
	alpha = 1.55f;
	beta = 0.f;

	//Set the projection matrix only once
	Projection = glm::perspective(glm::radians(parser.fovy), parser.width / parser.height, parser.nearPlane, parser.farPlane);

	//Initialize everything
	//InitializeShader(theProgram_Tex);

	for (unsigned i = 0; i < parser.lights.size() + parser.objects.size(); i++)
		vtx_count.push_back(*new unsigned);
	for (unsigned i = 0; i < parser.objects.size(); i++)
		normals_count.push_back(*new unsigned);

	InitializeLights();
	InitializeProgram();
	InitializeBuffers();

	unsigned i = 0;
	for (; i < parser.objects.size(); i++)
	{
		transform.push_back(glm::mat4(1.f));
		TransfMatrices(i);
	}
	for (unsigned j = 0; j < parser.lights.size(); j++)
	{
		transform.push_back(glm::mat4(1.f));
		TransfMatrices(j + i);
	}

	SetLightPosUniforms();
	InitializeUniforms();

	float using_tex = -1.f;
	if (using_texture)
		using_tex = 1.f;

	GLuint loc_tex = glGetUniformLocation(theProgram_Tex, "using_tex");
	glUniform1f(loc_tex, using_tex);
}

//Called to update the scene
void Scene::update()
{
	//Reset

	InitializeBuffers();


	change_subdiv = keep;
}

//Called to update the display.
//You should call SDL_GL_SwapWindow after all of your rendering to display what you rendered.
void Scene::display(SDL_Window* window)
{
	//Clear
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Get the updated view matrix
	glm::mat4 View = UpdateCameraViewMtx();
	std::vector<glm::mat4> MVP;


	unsigned j = 0;
	unsigned i = 0;

	//Get the transform matrices for the lights and shapes
	for (; i < parser.objects.size(); i++)
	{
		if (!parser.objects[i].anims.empty())
			TransfMatrices(i);
	}
	for (unsigned j = 0; j < parser.lights.size(); j++)
	{
		if (!parser.lights[j].anims.empty())
			TransfMatrices(j + i);
	}


	//Camera position
	float camera_pos[3];
	camera_pos[0] = cam_radius * sinf(alpha) * sinf(beta);
	camera_pos[1] = cam_radius * cosf(alpha);
	camera_pos[2] = cam_radius * sinf(alpha) * cosf(beta);


	glUseProgram(theProgram_Tex);
	
	//Set camera position as uniform
	GLuint cam_loc = glGetUniformLocation(theProgram_Tex, "cam_pos");
	glUniform3fv(cam_loc, 1, camera_pos);

	//If the texture is being used, set using_tex float to 1 (use it as a boolean)
	float using_tex = -1.f;
	if (using_texture)
		using_tex = 1.f;

	GLuint loc_tex = glGetUniformLocation(theProgram_Tex, "using_tex");
	glUniform1f(loc_tex, using_tex);



	//Bind the MVPs for each light
	size_t max = parser.objects.size() + parser.lights.size();
	i = static_cast<unsigned>(parser.objects.size());


	//Draw the LIGHTS
	for (int k = 0; i < max; i++)
	{
		MVP.push_back(Projection * View * transform[i]);

		{
			//The vao that contains the shapes
			glBindVertexArray(AO_N[i].vao);

			//Set the uniform matrix MVP
			GLuint unif_loc_shape = glGetUniformLocation(theProgram_Tex, "MVP");
			glUniformMatrix4fv(unif_loc_shape, 1, GL_FALSE, &(MVP[j++])[0][0]);

			// Draw
			glDrawArrays(GL_TRIANGLES, 0, static_cast<GLsizei>(vtx_count[i]));

			// Unbind
			glBindVertexArray(0);
		}
	}
	



	//Set the wireframe or full mode
	if(wireframe_mode)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);


	//Bind the MVPs for each shape
	glm::mat4 l_MVP[8]{ glm::mat4(1.f) };
	for(unsigned i = 0; i < j; i++)
	{
		l_MVP[i] = MVP[i];
	}

	SetLightPosUniforms();

	i = 0;
	//Draw the SHAPES
	for (; i < parser.objects.size(); i++)
	{
		MVP.push_back(Projection * View * transform[i]);
		glm::mat4 M2W = transform[i];
		{
			//The vao that contains the shapes
			glBindVertexArray(AO_S[i].vao);

			//Set the uniform matrix MVP
			GLuint unif_loc_shape = glGetUniformLocation(theProgram_Tex, "MVP");
			glUniformMatrix4fv(unif_loc_shape, 1, GL_FALSE, &(MVP[j++])[0][0]);

			//Set the uniform matrix MVP of all the lights
			GLuint loc_model = glGetUniformLocation(theProgram_Tex, "M2W");
			glUniformMatrix4fv(loc_model, 1, GL_FALSE, &(M2W)[0][0]);

			//Set the uniform view matrix
			GLuint loc_view = glGetUniformLocation(theProgram_Tex, "viewMtx");
			glUniformMatrix4fv(loc_view, 1, GL_FALSE, &(View)[0][0]);

			// Draw
			glDrawArrays(GL_TRIANGLES, 0, static_cast<GLsizei>(vtx_count[i]));

			// Unbind
			glBindVertexArray(0);
		}
	}



	j = static_cast<unsigned>(parser.lights.size());
	//Draw the NORMALS
	if (draw_normals)
	{
		//Change the program and the shader for drawing the normals
		// Bind the glsl program and this object's VAO
		
		for (unsigned i = 0; i < parser.objects.size(); i++)
		{
			//The vao that contains the normals
			glBindVertexArray(AO_N[i].vao);

			//Set the uniform matrix MVP
			GLuint unif_loc = glGetUniformLocation(theProgram_Tex, "MVP");
			glUniformMatrix4fv(unif_loc, 1, GL_FALSE, &(MVP[j++])[0][0]);

			// Draw
			glDrawArrays(GL_LINES, 0, static_cast<GLsizei>(normals_count[i]));

			// Unbind
			glBindVertexArray(0);
		}
	}

 	glUseProgram(0);

	SDL_GL_SwapWindow(window);
}

//Called to clean up the scene before every update and at the end of the program
void Scene::BufferCleanup()
{
	//Delete the buffers
	unsigned i = 0;
	for (; i < parser.objects.size(); i++)
	{
		// Delete the VBOs
		glDeleteBuffers(7, AO_S[i].BufferObjects);
		glDeleteBuffers(7, AO_N[i].BufferObjects);
		

		// Delete the VAO
		glDeleteVertexArrays(1, &AO_S[i].vao);
		glDeleteVertexArrays(1, &AO_N[i].vao);
	}

	for (; i < parser.lights.size() + parser.objects.size(); i++)
	{
		// Delete the VBOs
		glDeleteBuffers(7, AO_N[i].BufferObjects);

		// Delete the VAO
		glDeleteVertexArrays(1, &AO_N[i].vao);
	}
}

void Scene::cleanup()
{
	// Delete the program
	glDeleteProgram(theProgram_Tex);

	//Delete the buffers
	BufferCleanup();

	lights.clear();
	vtx_count.clear();
	normals_count.clear();
}



#undef main
int main(int argc, char* args[])
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		std::cout << "Could not initialize SDL: " << SDL_GetError() << std::endl;
		exit(1);
	}

	SDL_Window* window = SDL_CreateWindow("CS300", 100, 100, WIDTH, HEIGHT, SDL_WINDOW_OPENGL);
	if (window == nullptr)
	{
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		exit(1);
	}

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	SDL_GLContext context_ = SDL_GL_CreateContext(window);
	if (context_ == nullptr)
	{
		SDL_DestroyWindow(window);
		std::cout << "SDL_GL_CreateContext Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		exit(1);
	}

	glewExperimental = true;
	if (glewInit() != GLEW_OK)
	{
		SDL_GL_DeleteContext(context_);
		SDL_DestroyWindow(window);
		std::cout << "GLEW Error: Failed to init" << std::endl;
		SDL_Quit();
		exit(1);
	}

#if _DEBUG
	glEnable(GL_DEBUG_OUTPUT);
	glDebugMessageCallback(MessageCallback, 0);
#endif

	// print GPU data
	std::cout << "GL_VENDOR: " << glGetString(GL_VENDOR) << std::endl;
	std::cout << "GL_RENDERER: " << glGetString(GL_RENDERER) << std::endl;
	std::cout << "GL_VERSION: " << glGetString(GL_VERSION) << std::endl;

	GLint totalMemKb = 0;
	glGetIntegerv(GL_GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX, &totalMemKb);
	std::cout << "GL_GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX: " << totalMemKb << std::endl;
	glGetIntegerv(GL_GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX, &totalMemKb);
	std::cout << "GL_GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX: " << totalMemKb << std::endl;

	std::cout << std::endl
		<< "Extensions: "
		<< std::endl;
	int numExtensions;
	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);
	for (int i = 0; i < numExtensions; i++)
	{
		std::cout << glGetStringi(GL_EXTENSIONS, i) << std::endl;
	}

	Scene this_scene;
	this_scene.init();


	SDL_Event event;
	while (!this_scene.quit)
	{
		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_QUIT:
				this_scene.quit = true;
				break;
			case SDL_KEYDOWN:
				this_scene.GetInput(event);
				break;
			}
		}

		this_scene.display(window);
	}

	this_scene.cleanup();

	SDL_GL_DeleteContext(context_);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}
