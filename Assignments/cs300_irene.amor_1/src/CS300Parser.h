#pragma once

#include <glm/glm.hpp>

#include <string>
#include <vector>

#include "animations.h"

class CS300Parser
{
  public:
    void LoadDataFromFile(const char * filename);

    float     fovy      = 60.0f;
    float     width     = 16.0f;
    float     height    = 9.0f;
    float     nearPlane = 1.0f;
    float     farPlane  = 500.0f;
    glm::vec3 camPos;
    glm::vec3 camTarget;
    glm::vec3 camUp;

    struct Transform
    {
        std::string name;

        std::string mesh;

        glm::vec3 pos;
        glm::vec3 rot;
        glm::vec3 sca;
        float     ns = 10.0f;

        std::vector<Animations::Anim> anims;
    };

    std::vector<Transform> objects;

    struct Light
    {
        glm::vec3 pos;
        glm::vec3 dir;
        glm::vec3 col;
        glm::vec3 att;
        float     amb     = 0.0f;
        float     inner   = 0.0f;
        float     outer   = 30.0f;
        float     falloff = 1.0f;

        std::string type = "POINT";

        std::vector<Animations::Anim> anims;
    };
    std::vector<Light> lights;


    static glm::vec2 ReadVec2(std::ifstream& f);
    static glm::vec3 ReadTriangle(std::ifstream& f);
    static glm::vec3 ReadVec3(std::ifstream & f);

private:
    static float     ReadFloat(std::ifstream & f);
};