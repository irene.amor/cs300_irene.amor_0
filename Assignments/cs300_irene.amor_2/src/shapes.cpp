#include "scene.h"


// * * * * * * * * * * * *
// FUNCTIONS FOR NORMALS
// * * * * * * * * * * * *

Normal::Normal()
{
	vector = glm::vec4(0.f, 0.f, 0.f, 0.f);
	P0 = glm::vec4(0.f, 0.f, 0.f, 1.f);
	P1 = glm::vec4(0.f, 0.f, 0.f, 1.f);
	color = glm::vec3(0.f, 0.f, 0.f);
}


/**
* @brief Calculates the points of the line segment that represents the vectors
*
* @param average: whether the vectors are the averaged or the face normals, tangents and bitangents
*
*/
void Vertex::SetVectorsInfo(bool average)
{
	//Get the max and min values to do the switch
	int typeMin = faceNormal;
	int typeMax = faceBitangent;

	if (average == true)
	{
		typeMin = aveNormal;
		typeMax = aveBitangent;
	}

	for (int type = typeMin; type <= typeMax; type++)
	{
		//Find the vector
		Normal* vec = nullptr;
		if (average)
		{
			switch (type)
			{
			case aveNormal:
				vec = &ave_normal;
				break;
			case aveTangent:
				vec = &ave_tangent;
				break;
			case aveBitangent:
				vec = &ave_bitangent;
				break;
			}
		}
		else
		{
			switch (type)
			{
			case faceNormal:
				vec = &face_normal;
				break;
			case faceTangent:
				vec = &face_tangent;
				break;
			case faceBitangent:
				vec = &face_bitangent;
				break;
			}
		}

		//Calculate the points of the line segment that represents the vector
		if (vec)
		{
			if (vec->vector != glm::vec4(0.f, 0.f, 0.f, 0.f))
				vec->vector = normalize(vec->vector) * 0.2f;


			vec->P0 = glm::vec4(pos[0], pos[1], pos[2], 0.f);
			vec->P1 = vec->vector + vec->P0;
		}
	}
}

/**
* @brief Asserts that the normal, tangent and bitangent are perpendicular
*
* @param average: whether the vectors are the averaged or the face normals, tangents and bitangents
*
*/
void Vertex::CheckTangentPerpendicularity(bool average)
{
	//Get the vectors as they are
	glm::vec3 normal = glm::vec3(face_normal.vector);
	glm::vec3 tangent = glm::vec3(face_tangent.vector);
	glm::vec3 bitangent = glm::vec3(face_bitangent.vector);

	if (average)
	{
		normal = glm::vec3(ave_normal.vector);
		tangent = glm::vec3(ave_tangent.vector);
		bitangent = glm::vec3(ave_bitangent.vector);
	}

	// Set normal to (0,0,1) when lenght is 0
	if (glm::length(normal) == 0)
	{
		normal = { 0.f, 0.f, 1.f };
		tangent = { 1.f, 0.f, 0.f };
		bitangent = { 0.f, 1.f, 0.f };
	}


	//Tangent is not perpendicular
	if (dot(tangent, normal) != 0)
	{
		glm::vec3 newTangent = glm::cross(bitangent, normal);
		tangent = newTangent;
	}
	//Tangent is not perpendicular
	else if (dot(bitangent, normal) != 0)
	{
		glm::vec3 newTangent = glm::cross(tangent, normal);
		bitangent = newTangent;
	}

	//Orthonormalize the tangent and bitangent
	{

		// Gram-Schmidt orthogonalization of tangent respect to normal and normalize tangent
		glm::vec3 projNT = static_cast<float>((dot(tangent, normal)) / dot(normal, normal)) * normal;

		glm::vec3 newTangent = tangent - projNT;
		tangent = newTangent;


		// Compute the new perpendicular bitangent maintaining the original handeness of the previously computed bitangent
		glm::vec3 newBitangent = glm::cross(normal, newTangent);


		if (dot(bitangent, newBitangent) < 0)
			bitangent = cross(tangent, normal);
		else
			bitangent = cross(normal, tangent);
	}


	if (average)
	{
		ave_normal.vector = glm::vec4(normalize(normal), 0.f);
		ave_tangent.vector = glm::vec4(normalize(tangent), 0.f);
		ave_bitangent.vector = glm::vec4(normalize(bitangent), 0.f);
	}
	else
	{
		face_normal.vector = glm::vec4(normalize(normal), 0.f);
		face_tangent.vector = glm::vec4(normalize(tangent), 0.f);
		face_bitangent.vector = glm::vec4(normalize(bitangent), 0.f);
	}
}

/**
* @brief Computes the perpendicular normal, tangent and bitangent
*
* @param vertices: the vertices that compose the shape to compute the tangent basis for
*
*/
void Vertex::ComputeTangentBasis(std::vector<Vertex> &vertices) const
{
	// Reset tangents and bitangents to zero vectors
	for (unsigned i = 0; i < vertices.size(); i++)
	{
		vertices[i].face_tangent.vector = glm::vec4(0.f, 0.f, 0.f, 0.f);
		vertices[i].face_bitangent.vector = glm::vec4(0.f, 0.f, 0.f, 0.f);

		vertices[i].ave_tangent.vector = glm::vec4(0.f, 0.f, 0.f, 0.f);
		vertices[i].ave_bitangent.vector = glm::vec4(0.f, 0.f, 0.f, 0.f);
	}

	// Loop through the triangles (using the index buffer)
	for (unsigned idx = 0; idx < vertices.size(); idx += 3)
	{
		unsigned idx_1 = idx + 1;
		unsigned idx_2 = idx + 2;

		glm::vec3 P0{ vertices[idx].pos[0], vertices[idx].pos[1], vertices[idx].pos[2] };
		glm::vec3 P1{ vertices[idx_1].pos[0], vertices[idx_1].pos[1], vertices[idx_1].pos[2] };
		glm::vec3 P2{ vertices[idx_2].pos[0], vertices[idx_2].pos[1], vertices[idx_2].pos[2] };

		// Solve equations to find T and B for this triangle
		glm::vec3 V1 = P1 - P0;
		glm::vec3 V2 = P2 - P0;

		glm::vec2 UV1 = { (vertices[idx_1].UV[0] - vertices[idx].UV[0]) / 255.f,
							(vertices[idx_1].UV[1] - vertices[idx].UV[1]) / 255.f };
		glm::vec2 UV2 = { (vertices[idx_2].UV[0] - vertices[idx].UV[0]) / 255.f ,
							(vertices[idx_2].UV[1] - vertices[idx].UV[1]) / 255.f };


		float denom = (UV1.y * UV2.x - UV2.y * UV1.x);
		glm::vec3 T, B;

		if (denom == 0.f)	// || vertices[i].face_normal.vector == glm::vec4(0.f,0.f,0.f,0.f))
		{
			T = { 1.f, 0.f, 0.f };
			B = { 0.f, 1.f, 0.f };
		}
		else
		{
			T = (UV1.y * V2 - UV2.y * V1) / denom;
			B = (UV2.x * V1 - UV1.x * V2) / denom;
		}

		// Accumulate tangent/bitangent for the 3 vertices of the triangle (to average after)
		for (unsigned j = 0; j < 3; j++)
		{
			unsigned idx_ = idx + j;
			vertices[idx_].face_tangent.vector = glm::vec4(T, 0.f);
			vertices[idx_].face_bitangent.vector = glm::vec4(B, 0.f);
		}
	}


	// Loop through every vertex
	for (unsigned i = 0; i < vertices.size(); i++)
	{
		vertices[i].CheckTangentPerpendicularity(false);
		vertices[i].SetVectorsInfo(false);
	}
}


/**
* @brief Calculates the normals in the face made from the three inputted points
*
* @param P0, P1, P2: the points that make up the plane
*
*/
void Vertex::SetAllNormals(Vertex& P0, Vertex& P1, Vertex& P2)
{
	//Calculate normals for every vertex
	P0.CalculateFaceNormal(P2, P1);
}

/**
* @brief Calculates the normal at "this" vertex
*
* @param P1, P2: the other 2 points on the plane
*
*/
void Vertex::CalculateFaceNormal(Vertex& P1, Vertex& P2)
{
	//Get the vectors that start at "this" point
	glm::vec3 V1, V2, cross;
	for (int i = 0; i < 3; i++)
	{
		face_normal.P0[i] = pos[i];
		P1.face_normal.P0[i] = P1.pos[i];
		P2.face_normal.P0[i] = P2.pos[i];
		
		V1[i] = P1.pos[i] - pos[i];
		V2[i] = P2.pos[i] - pos[i];
	}

	//Get the normal
	cross = glm::cross(V1, V2);

	//Change the lenght of the vector
	//Make sure a zero vector does not call normalize
	if (cross != glm::vec3(0.f, 0.f, 0.f))
		cross = normalize(cross);

	//Set the normal and a point in that line
	face_normal.vector = glm::vec4(cross, 0.f);
	P1.face_normal.vector = glm::vec4(cross, 0.f);
	P2.face_normal.vector = glm::vec4(cross, 0.f);
}

/**
* @brief Calculates the average normal of "this" vertex
*
* @param shape: all the vertices that make up a shape
*
*/
void Vertex::CalculateAverageVectors(std::vector<Vertex> shape)
{

	std::vector<Vertex> repeated_vtx;
		
	glm::vec4 averageNormal = { 0.f, 0.f, 0.f, 0.f };
	glm::vec4 averageTangent = { 0.f, 0.f, 0.f, 0.f };
	glm::vec4 averageBitangent = { 0.f, 0.f, 0.f, 0.f };


	for (int i = 0; i < shape.size(); i++)
	{
		//Find "this" in all the normal_P0
		bool equal = true;
		for (int j = 0; j < 3; j++)
		{
			if (shape[i].pos[j] != this->pos[j] || shape[i].face_normal.vector == glm::vec4(0.f, 0.f, 0.f, 0.f))
				equal = false;
		}

		//Put all the repeated vertices into a vector
		if (equal)
		{
			for (int k = 0; k < repeated_vtx.size(); k++)
			{
				equal = true;

				if (shape[i].face_normal.vector != repeated_vtx[k].face_normal.vector)
					equal = false;
					
			}

			if (!equal || repeated_vtx.empty())
				repeated_vtx.push_back(shape[i]);
		}

	}


	//Add them and divide by the number of vectors
	for (int i = 0; i < repeated_vtx.size(); i++)
	{
		averageNormal += repeated_vtx[i].face_normal.vector;
		averageTangent += repeated_vtx[i].face_tangent.vector;
		averageBitangent += repeated_vtx[i].face_bitangent.vector;
	}

	//Average the x,y,z coordinates
	averageNormal /= repeated_vtx.size();
	averageTangent /= repeated_vtx.size();
	averageBitangent /= repeated_vtx.size();

	//Change the lenght of the vector
	if (averageNormal != glm::vec4(0.f, 0.f, 0.f, 0.f))
		averageNormal = normalize(averageNormal);
	else
		averageNormal = glm::vec4(0.f, 0.f, 1.f, 0.f);

	if (averageTangent != glm::vec4(0.f, 0.f, 0.f, 0.f))
		averageTangent = normalize(averageTangent);
	else
		averageTangent = glm::vec4(1.f, 0.f, 0.f, 0.f);

	if (averageBitangent != glm::vec4(0.f, 0.f, 0.f, 0.f))
		averageBitangent = normalize(averageBitangent);
	else
		averageBitangent = glm::vec4(0.f, 1.f, 0.f, 0.f);


	ave_normal.vector = averageNormal;
	ave_tangent.vector = averageTangent;
	ave_bitangent.vector = averageBitangent;
			
	CheckTangentPerpendicularity(true);
	SetVectorsInfo(true);
}


/**
* @brief Gets the corresponding normal and calculates two points on that line
*		 for every vertex in the shape
*
* @param shape:		all the vertices that make up a shape
* @param average:	whether to get the info for the averaged or face normals
* 
* @return: vector with the two points in each normal saved as (P0_1, P1_1, P0_2, P1_2 ...)
*
*/
std::vector<Vertex> Shape::GetNormals(std::vector<Vertex> shape, bool average)
{
	int typeMin = faceNormal;
	int typeMax = faceBitangent;

	if (average == true)
	{
		typeMin = aveNormal;
		typeMax = aveBitangent;
	}

	//Create a vector to store the two points that make the normal of each vertex in the shape
	std::vector<Vertex> normals;

	size_t max = shape.size();
	for (unsigned i = 0; i < max; i++)
	{
		for (int type = typeMin; type <= typeMax; type++)
		{
			Normal* vec = nullptr;

			if (average)
			{
				switch(type)
				{
				case aveNormal:
					vec = &shape[i].ave_normal;
					break;
				case aveTangent:
					vec = &shape[i].ave_tangent;
					break;
				case aveBitangent:
					vec = &shape[i].ave_bitangent;
					break;
				}
			}
			else
			{
				switch (type)
				{
				case faceNormal:
					vec = &shape[i].face_normal;
					break;
				case faceTangent:
					vec = &shape[i].face_tangent;
					break;
				case faceBitangent:
					vec = &shape[i].face_bitangent;
					break;
				}
			}



			//Copy the positions and colors
			Vertex P0, P1;
			if (vec)
			{
				for (unsigned j = 0; j < 3; j++)
				{
					P0.pos[j] = vec->P0[j];
					P1.pos[j] = vec->P1[j];
				}

				P0.pos[3] = 1.f;
				P1.pos[3] = 1.f;
				for (unsigned j = 0; j < 3; j++)
				{
					P0.color[j] = vec->color[j];
					P1.color[j] = vec->color[j];
				}
			}
			//Add them into the vertex
			normals.push_back(P0);
			normals.push_back(P1);
		}
	}

	return normals;
}


// - - - - - - - - - - - -
// FUNCTIONS FOR COLORS
// - - - - - - - - - - - -

/**
* @brief Sets the colors of every vertex in the shape so that the shape has green and red colors
*
* @param shape: all the vertices that make up a shape
*
*/

void Shape::SetColors(std::vector<Vertex>& shape)
{
	//Get the color at each vertex (green and red)
	size_t max_vtx = shape.size();
	for (unsigned i = 0; i < max_vtx; i++)
	{
		shape[i].color[0] = (shape[i].UV[0] / 255.f);
		shape[i].color[1] = (shape[i].UV[1] / 255.f);
		shape[i].color[2] = 0;
		
		shape[i].face_normal.color = glm::vec3(0.f, 0.f, 1.f);
		shape[i].face_tangent.color = glm::vec3(1.f, 0.f, 0.f);
		shape[i].face_bitangent.color = glm::vec3(0.f, 1.f, 0.f);

		shape[i].ave_normal.color = glm::vec3(0.f, 0.f, 1.f);
		shape[i].ave_tangent.color = glm::vec3(1.f, 0.f, 0.f);
		shape[i].ave_bitangent.color = glm::vec3(0.f, 1.f, 0.f);
	}


}


// - - - - - - - - - - - -
// FUNCTIONS FOR UVs
// - - - - - - - - - - - -

/**
* @brief Calculates the cylindrical coordinates for the UV of the vertex
*
* @param vtx:	the vertex to calculate the UVs for
* @param angle: the angle vtx is at in the cylinder
*
*/
void Shape::CylindricalCoords_UV(Vertex& vtx, float angle)
{
	//Calculate the cylindrical coordinates
	//given the angle and the position
	vtx.UV[0] = static_cast<unsigned char>((angle / (2 * pi)) * 255);
	vtx.UV[1] = static_cast<unsigned char>((vtx.pos[1] + 0.5) * 255);
}

/**
* @brief Calculates the spherical coordinates for the UV of the vertex
*
* @param vtx:		the vertex to calculate the UVs for
* @param angle_H:	the horizontal angle vtx is at in the sphere
* @param angle_V:	the vertical angle vtx is at in the sphere
*
*/
void Shape::SphericalCoords_UV(Vertex& vtx, float angle_H, float angle_V)
{
	//Calculate the spherical coordinates
	//given the two angles
	float a = -angle_H / (2 * pi);
	vtx.UV[0] = static_cast<unsigned char>((1+a) * 255);

	float b = (angle_V + (pi/2)) / (pi);
	vtx.UV[1] = static_cast<unsigned char>((b) * 255);
}




// - - - - - - - - - - - - - - - - - - - - - - - -
// FUNCTION TO CREATE A VERTEX GIVEN THE POSITION 
// - - - - - - - - - - - - - - - - - - - - - - - -

/**
* @brief Create a vertex given its position
*
* @param x,y,z: the coordinates for the position
* 
* @return: the Vertex containing only the position
*
*/
Vertex Shape::CreateVertex(float x, float y, float z)
{
	Vertex v;
	
	//Sanity check: round down super small numbers to zero
	if (abs(x) < FLT_EPSILON)
		x = 0.f;
	if (abs(y) < FLT_EPSILON)
		y = 0.f;
	if (abs(z) < FLT_EPSILON)
		z = 0.f;

	v.pos[0] = x;
	v.pos[1] = y;
	v.pos[2] = z;
	v.pos[3] = 1.f;

	return v;
}



// - - - - - - - - - - - - - - - - - 
// FUNCTIONS FOR CALCULATING SHAPES 
// - - - - - - - - - - - - - - - - -

/**
* @brief Calls the necessary function to draw the specified shape
*
* @param mesh:			the (file) name of the mesh
* @param change_subdiv: whether to increase/decrease/keep the number
*						of subdivisions of the shape
*
* @return: the vertices that make up the shape
*/
std::vector<Vertex> Shape::DrawShape(std::string mesh, unsigned change_subdiv, float shininess)
{
	if (pi == 0.f)
		pi = glm::pi<float>();


	std::vector<Vertex> vtx;
	const char* mesh_name = mesh.c_str();

	//Given a mesh that needs to be loaded
	if (mesh.find("data/meshes/", 0) == 0)
	{
		vtx = DrawFromMesh(mesh);
	}
	//Or calculate the mesh
	else if (!strcmp(mesh_name, "PLANE"))
	{
		//Plane
		vtx = DrawPlane();
	}
	else if (!strcmp(mesh_name, "CUBE"))
	{
		//Cube
		vtx = DrawCube();
	}
	else if (!strcmp(mesh_name, "CONE"))
	{
		//Cone
		vtx = DrawCone(change_subdiv);
	}
	else if (!strcmp(mesh_name, "CYLINDER"))
	{
		//Cylinder
		vtx = DrawCylinder(change_subdiv);
	}
	else if (!strcmp(mesh_name, "SPHERE"))
	{
		//Sphere
		vtx = DrawSphere(change_subdiv);
	}

	//Set shininess of the shape
	for (int i = 0; i < vtx.size(); i++)
	{
		vtx[i].shininess = shininess;
	}


	return vtx;
}

/**
* @brief Loads the provided mesh
*
* @param filename: the filename of the mesh
*
* @return: the vertices that make up the shape
*/
std::vector<Vertex> Shape::DrawFromMesh(std::string filename)
{
	//Read the input from a file
	std::ifstream inFile(filename);

	//Sanity check: could not open the file
	if (!inFile.is_open())
	{
		std::cout << "Could not open input file " << filename << std::endl;
		exit(0);
	}

	
	std::string str;								//Current line
	std::vector<std::array<float, 4>> mesh_pos;		//The position of the mesh at the vertex
	std::vector<std::array<float, 2>> mesh_tex;		//The texture of the mesh at the vertex
	std::vector<std::array<float, 3>> mesh_normal;	//The normals of the mesh at the vertex
	std::vector<Vertex> vertices;					//All the vertices with their positions, texture coords and normals


	//Load everything
	while (!inFile.eof())
	{
		str = "";
		inFile >> str;

		std::string id = str;

		//Get the position
		if (id == "v")
		{
			glm::vec3 vector = CS300Parser::ReadVec3(inFile);
			std::array<float, 4> pos{ { vector.x, vector.y, vector.z, 1.f } };
			mesh_pos.push_back(pos);
		}
		//Get the texture
		else if (id == "vt")
		{
			glm::vec2 vector = CS300Parser::ReadVec2(inFile);
			std::array<float, 2> tex{ { vector.x, vector.y } };
			mesh_tex.push_back(tex);
		}
		//Get the normal
		else if (id == "vn")
		{
			glm::vec3 vector = CS300Parser::ReadVec3(inFile);
			std::array<float, 3> normal{ { vector.x, vector.y, vector.z } };
			mesh_normal.push_back(normal);
		}
		//Put together the inputs into the correct vertices
		else if (id == "f")
		{
			//Get the index of the info for the current 3 vertices (pos/tex/normal)
			//Lines such as: 1/1/1 2/2/2 3/3/3
			std::vector<glm::vec3> vecs;
			vecs.push_back(CS300Parser::ReadTriangle(inFile));
			vecs.push_back(CS300Parser::ReadTriangle(inFile));
			vecs.push_back(CS300Parser::ReadTriangle(inFile));

			//Get the current vertex info
			for (int j = 0; j < 3; j++)
			{
				Vertex v;
				//Position
				for (unsigned i = 0; i < 4; i++)
				{
					unsigned idx = static_cast<unsigned char>(vecs[j][0] - 1);
					v.pos[i] = mesh_pos[idx][i];
				}

				//Texture UV
				for (unsigned i = 0; i < 2; i++)
				{
					unsigned idx = static_cast<unsigned int>(vecs[j][1] - 1);
					v.UV[i] = static_cast<unsigned char>(mesh_tex[idx - 1][i] * 255);
				}

				//Normal
				glm::vec4 normal;
				for (unsigned i = 0; i < 3; i++)
				{
					unsigned idx_1 = static_cast<unsigned char>(vecs[j][0] - 1);
					v.face_normal.P0[i] = mesh_pos[idx_1][i];

					unsigned idx_2 = static_cast<unsigned char>(vecs[j][2] - 1);
					normal[i] = mesh_normal[idx_2][i];

					v.face_normal.color[i] = 1.f;
				}

				//Change the lenght of the normal vector
				normal = normalize(normal);


				v.face_normal.vector = normal;


				//Add into the vector

				vertices.push_back(v);
			}
		}
	}

	//Set the colors
	SetColors(vertices);


	Vertex v;
	v.ComputeTangentBasis(vertices);

	for (int i = 0; i < vertices.size(); i++)
	{
		vertices[i].ave_normal = vertices[i].face_normal;
		vertices[i].ave_tangent = vertices[i].face_tangent;
		vertices[i].ave_bitangent = vertices[i].face_bitangent;
		vertices[i].SetVectorsInfo(true);
	}


	return vertices;
}

/**
* @brief Creates a plane
*
* @return: the vertices that make up the shape
*/
std::vector<Vertex> Shape::DrawPlane()
{
	//The plane does not change, avoid calculating it more than once
	if (plane.empty())
		CalculatePlane();

	return plane;
}

/**
* @brief Calculates the vertices of a plane
*
* @return: the vertices that make up the shape
*/
void Shape::CalculatePlane()
{
	//Get the vertices that form the plane
	Vertex BR = { { 0.5f, -0.5f, 0.f, 1.0f }, {}, { 1, 0 } };
	Vertex BL = { {-0.5f, -0.5f, 0.f, 1.0f }, {}, { 0, 0 } };
	Vertex TR = { {0.5f, 0.5f, 0.f, 1.0f }, {}, { 1, 0 } };
	Vertex TL = { {-0.5f, 0.5f, 0.f, 1.0f }, {}, { 0, 0 } };

	//Triangluate the vertex positions
	plane = { BR, TR, BL, TL, BL, TR };

	//Calculate normals
	plane[0].CalculateFaceNormal(TR, BL);  //BR
	plane[1].CalculateFaceNormal(BL, BR);  //TR
	plane[2].CalculateFaceNormal(BR, TR);  //BL
								
	plane[3].CalculateFaceNormal(BL, TR);  //TL
	plane[4].CalculateFaceNormal(TR, TL);  //BL
	plane[5].CalculateFaceNormal(TL, BL);  //TR

	Vertex v;
	v.ComputeTangentBasis(plane);

	for (int i = 0; i < 6; i++)
	{
		//Calculate UVs
		plane[i].UV[0] = static_cast<unsigned char>((0.5f + plane[i].pos[0]) * 255);
		plane[i].UV[1] = static_cast<unsigned char>((0.5f + plane[i].pos[1]) * 255);

		//Calculate average normals
		plane[i].CalculateAverageVectors(plane);

	}

	SetColors(plane);
}

/**
* @brief Creates a cube
*
* @return: the vertices that make up the shape
*/
std::vector<Vertex> Shape::DrawCube()
{
	//The cube does not change, avoid calculating it more than once
	if (cube.empty())
		CalculateCube();

	return cube;
}

/**
* @brief Calculates the vertices of a cube
*
* @return: the vertices that make up the shape
*/
void Shape::CalculateCube()
{
	//Get the vertices that form the cube
	//Front face
	Vertex TLF{ {-0.5f, 0.5f, 0.5f, 1.0f}};
	Vertex TRF{ {0.5f, 0.5f, 0.5f, 1.0f}};
	Vertex BLF{ {-0.5f, -0.5f, 0.5f, 1.0f}};
	Vertex BRF{ {0.5f, -0.5f, 0.5f, 1.0f}};

	//Back face
	Vertex TLB{ {-0.5f, 0.5f, -0.5f, 1.0f}};
	Vertex TRB{ {0.5f, 0.5f, -0.5f, 1.0f}};
	Vertex BLB{ {-0.5f, -0.5f, -0.5f, 1.0f}};
	Vertex BRB{ {0.5f, -0.5f, -0.5f, 1.0f}};

	Vertex n;

	int v = 0;
	//BACK face
	{
		//Vertex positions
		cube.push_back(TLB);	cube.push_back(TRB);	cube.push_back(BLB);
		cube.push_back(BRB);	cube.push_back(BLB);	cube.push_back(TRB);

		//Calculate UVs
		for (int i = 0; i < 6; i++)
		{
			cube[i].UV[0] = static_cast<unsigned char>((-(cube[i].pos[0] - 0.5)) * 255);
			cube[i].UV[1] = static_cast<unsigned char>((0.5 + cube[i].pos[1]) * 255);
		}

		//Calculate normals
		n.SetAllNormals(cube[v++], cube[v++], cube[v++]);
		n.SetAllNormals(cube[v++], cube[v++], cube[v++]);
	}

	//FRONT face
	{
		//Vertex positions
		cube.push_back(TRF);	cube.push_back(TLF);	cube.push_back(BLF);
		cube.push_back(TRF);	cube.push_back(BLF);	cube.push_back(BRF);

		//Calculate UVs
		for (int i = 6; i < 12; i++)
		{
			cube[i].UV[0] = static_cast<unsigned char>((0.5 + cube[i].pos[0]) * 255);
			cube[i].UV[1] = static_cast<unsigned char>((0.5 + cube[i].pos[1]) * 255);
		}

		//Calculate normals
		n.SetAllNormals(cube[v++], cube[v++], cube[v++]);
		n.SetAllNormals(cube[v++], cube[v++], cube[v++]);
	}


	//LEFT face
	{
		//Vertex positions
		cube.push_back(TLB);	cube.push_back(BLB);	cube.push_back(BLF);
		cube.push_back(TLB);	cube.push_back(BLF);	cube.push_back(TLF);

		//Calculate UVs
		for (int i = 12; i < 18; i++)
		{
			cube[i].UV[0] = static_cast<unsigned char>((0.5 + cube[i].pos[2]) * 255);
			cube[i].UV[1] = static_cast<unsigned char>((0.5 + cube[i].pos[1]) * 255);
		}

		//Calculate normals
		n.SetAllNormals(cube[v++], cube[v++], cube[v++]);
		n.SetAllNormals(cube[v++], cube[v++], cube[v++]);
	}

	//RIGHT face
	{
		//Vertex positions
		cube.push_back(TRB);	cube.push_back(BRF);	cube.push_back(BRB);
		cube.push_back(TRB);	cube.push_back(TRF);	cube.push_back(BRF);

		//Calculate UVs
		for (int i = 18; i < 24; i++)
		{
			cube[i].UV[0] = static_cast<unsigned char>((-(cube[i].pos[2] - 0.5)) * 255);
			cube[i].UV[1] = static_cast<unsigned char>((0.5 + cube[i].pos[1]) * 255);
		}


		//Calculate normals
		n.SetAllNormals(cube[v++], cube[v++], cube[v++]);
		n.SetAllNormals(cube[v++], cube[v++], cube[v++]);
	}

	//TOP face
	{
		//Vertex positions
		cube.push_back(TRB);	cube.push_back(TLB);	cube.push_back(TLF);
		cube.push_back(TRB);	cube.push_back(TLF);	cube.push_back(TRF);

		//Calculate UVs
		for (int i = 24; i < 30; i++)
		{
			cube[i].UV[0] = static_cast<unsigned char>((0.5 + cube[i].pos[0]) * 255);
			cube[i].UV[1] = static_cast<unsigned char>((0.5 + cube[i].pos[2]) * 255);
		}

		//Calculate normals
		n.SetAllNormals(cube[v++], cube[v++], cube[v++]);
		n.SetAllNormals(cube[v++], cube[v++], cube[v++]);
	}

	//BOTTOM face
	{
		//Vertex positions
		cube.push_back(BLB);	cube.push_back(BRB);	cube.push_back(BLF);
		cube.push_back(BRF);	cube.push_back(BLF);	cube.push_back(BRB);

		//Calculate UVs
		for (int i = 30; i < 36; i++)
		{
			cube[i].UV[0] = static_cast<unsigned char>((-(cube[i].pos[0] - 0.5)) * 255);
			cube[i].UV[1] = static_cast<unsigned char>((0.5 + cube[i].pos[2]) * 255);
		}

		//Calculate normals
		n.SetAllNormals(cube[v++], cube[v++], cube[v++]);
		n.SetAllNormals(cube[v++], cube[v++], cube[v++]);
	}

	SetColors(cube);

	Vertex vtx;
	vtx.ComputeTangentBasis(cube);

	for (int i = 0; i < cube.size(); i++)
	{
		cube[i].CalculateAverageVectors(cube);
	}

}

/**
* @brief Calculates the vertices of a cone
*
* @param change_subdiv: whether to increase/decrease/keep the number
*						of subdivisions of the shape
* 
* @return: the vertices that make up the shape
*/
std::vector<Vertex> Shape::DrawCone(unsigned change_subdiv)
{
	//The number of subdivisions might need to change
	if (change_subdiv == increase)
		subdiv_cone++;
	else if (change_subdiv == decrease && subdiv_cone > 4)
		subdiv_cone--;

	//Get the center of the base of the cone
	std::vector<Vertex> cone;
	Vertex base_center = Vertex{ {0.f, -r, 0.f, 1.f} };
	base_center.UV[1] = 0;

	//Get the top of the cone
	Vertex top_vtx = Vertex{ {0.f, r, 0.f, 1.f} };
	top_vtx.UV[1] = 255;

	//Set the number of subdivisions
	int max_vtx = subdiv_cone;

	//The angle between the vertices depends on the number of subdivisions
	float angle = 2 * pi / max_vtx;

	Vertex n;
	//Calculate the info for the vertices
	int v = 0;
	for (int i = 0; i < max_vtx; i++)
	{
		int j = i + 1;

		//Get the two vertices that will need to be triangulated with
		//the center of the base and the top
		Vertex BL = CreateVertex(cosf(angle * i) * r, -0.5f, sinf(angle * i) * r);
		CylindricalCoords_UV(BL, angle * i);

		Vertex BR = CreateVertex(cosf(angle * j) * r, -0.5f, sinf(angle * j) * r);
		CylindricalCoords_UV(BR, angle * j);


		//Vertices for the BASE
		{
			//The u coordinate of the UV of the center of the base needs to change every time
			float a = ((angle * i) + (angle / 2)) / (2 * pi);
			base_center.UV[0] = static_cast<unsigned char>(a * 255);

			//Triangulate
			cone.push_back(base_center);
			cone.push_back(BL);
			cone.push_back(BR);
		}

		//Vertices for the SIDES 
		{
			//The u coordinate of the UV of the top needs to change every time
			float b = ((angle * i) + (angle / 2)) / (2 * pi);
			top_vtx.UV[0] = static_cast<unsigned char>(b * 255);

			//Triangulate
			cone.push_back(top_vtx);
			cone.push_back(BR);
			cone.push_back(BL);
		}

		//Calculate the normals
		n.SetAllNormals(cone[v++], cone[v++], cone[v++]);
		n.SetAllNormals(cone[v++], cone[v++], cone[v++]);
	}

	//Set the colors of each vertex
	SetColors(cone);


	Vertex vtx;
	vtx.ComputeTangentBasis(cone);

	//Calculate the average normals
	for (int i = 0; i < cone.size(); i++)
	{
		cone[i].CalculateAverageVectors(cone);
	}

	return cone;
}

/**
* @brief Calculates the vertices of a cylinder
*
* @param change_subdiv: whether to increase/decrease/keep the number
*						of subdivisions of the shape
*
* @return: the vertices that make up the shape
*/
std::vector<Vertex> Shape::DrawCylinder(unsigned change_subdiv)
{
	//The number of subdivisions might need to change
	if (change_subdiv == increase)
		subdiv_cylinder++;
	else if (change_subdiv == decrease && subdiv_cylinder > 4)
		subdiv_cylinder--;

	//Set the y coordinate for the top and bottom
	float top = r;
	float bot = -r;

	//Get the center of the base of the cylinder
	std::vector<Vertex> cylinder;
	Vertex base_center = Vertex{ {0.f, bot, 0.f, 1.f} };
	base_center.UV[1] = 0;

	//Get the center of the topp of the cylinder
	Vertex top_center= Vertex{ {0.f, top, 0.f, 1.f} };
	top_center.UV[1] = 255;

	//Set the number of subdivisions
	int max_vtx = subdiv_cylinder;

	//The angle between the vertices depends on the number of subdivisions
	float angle = 2 * pi / max_vtx;
	
	Vertex n;
	//Calculate the info for the vertices
	int v = 0;
	for (int i = 0; i < max_vtx; i++)
	{
		int j = i + 1;

		//Get the vertices that will need to be triangulated with
		//the center of the base and the top
		Vertex BL = CreateVertex(cosf(angle * i) * r, bot, sinf(angle * i) * r);
		CylindricalCoords_UV(BL, angle * i);

		Vertex BR = CreateVertex(cosf(angle * j) * r, bot, sinf(angle * j) * r);
		CylindricalCoords_UV(BR, angle * j);

		Vertex TL = CreateVertex(cosf(angle * i) * r, top, sinf(angle * i) * r);
		CylindricalCoords_UV(TL, angle * i);

		Vertex TR = CreateVertex(cosf(angle * j) * r, top, sinf(angle * j) * r);
		CylindricalCoords_UV(TR, angle * j);


		//Vertices for the BASE
		{
			//The u coordinate of the UV of the center of the base needs to change every time
			base_center.UV[0] = static_cast<unsigned char>(((angle * i) + (angle / 2)) / (2 * pi) * 255);

			//Triangulate
			cylinder.push_back(base_center);
			cylinder.push_back(BL);
			cylinder.push_back(BR);
		}

		//Vertices for the TOP 
		{
			//The u coordinate of the UV of the center of the top needs to change every time
			top_center.UV[0] = static_cast<unsigned char>(((angle * i) + (angle / 2)) / (2 * pi) * 255);

			//Triangulate
			cylinder.push_back(top_center);
			cylinder.push_back(TR);
			cylinder.push_back(TL);
		}

		//Calculate the normals
		n.SetAllNormals(cylinder[v++], cylinder[v++], cylinder[v++]);
		n.SetAllNormals(cylinder[v++], cylinder[v++], cylinder[v++]);


		//Vertices for the SIDES
		{
			//Triangulate
			cylinder.push_back(TL);
			cylinder.push_back(TR);
			cylinder.push_back(BR);

			//Triangulate
			cylinder.push_back(BR);
			cylinder.push_back(BL);
			cylinder.push_back(TL);
		}

		//Calculate the normals
		n.SetAllNormals(cylinder[v++], cylinder[v++], cylinder[v++]);
		n.SetAllNormals(cylinder[v++], cylinder[v++], cylinder[v++]);
	}

	//Set the colors of each vertex
	SetColors(cylinder);

	Vertex vtx;
	vtx.ComputeTangentBasis(cylinder);

	//Calculate the average normals
	for (int i = 0; i < cylinder.size(); i++)
	{
		cylinder[i].CalculateAverageVectors(cylinder);
	}

	return cylinder;
}

/**
* @brief Calculates the vertices of a sphere
*
* @param change_subdiv: whether to increase/decrease/keep the number
*						of subdivisions of the shape
*
* @return: the vertices that make up the shape
*/
std::vector<Vertex> Shape::DrawSphere(unsigned change_subdiv)
{
	if (pi == 0.f)
		pi = glm::pi<float>();

	//The number of subdivisions might need to change
	if (change_subdiv == increase)
		subdiv_sphere++;
	else if (change_subdiv == decrease && subdiv_sphere > 4)
		subdiv_sphere--;

	//Set the y coordinate for the top and bottom
	float bot = -r;
	float top = r;

	std::vector<Vertex> sphere;

	//Set the number of subdivisions and rings
	int max_vtx = subdiv_sphere;
	int rings = max_vtx / 2;

	//Get the angles
	float angle_H = 2 * pi / max_vtx;	//The initial angle on the horizontal
	float angle_diff = pi / rings;		//The "step" of the horizontal angle
	float current_angle_V = -pi/2;		//The initial angle on the vetical

	Vertex n;
	int v = 0;
	//Horizontal slices
	for (int i = 0; i < max_vtx; i++)
	{
		int j = i + 1;
		current_angle_V = -pi/2;

		//Vertical slices
		for (int k = 0; k < rings; k++)
		{
			//Get the y coordinates for the vertices at the top and at the bottom of the current ring
			float top = sinf(current_angle_V + angle_diff) * r;
			float bot = sinf(current_angle_V) * r;

			//Get the vertices that will need to be triangulated
			Vertex BL = CreateVertex(sinf(angle_H * i) * cosf(current_angle_V) * r, bot, cosf(current_angle_V) * cosf(angle_H * i) * r);
			SphericalCoords_UV(BL, angle_H * i, current_angle_V);
			Vertex BR = CreateVertex(sinf(angle_H * j) * cosf(current_angle_V) * r, bot, cosf(current_angle_V) * cosf(angle_H * j) * r);
			SphericalCoords_UV(BR, angle_H * j, current_angle_V);

			Vertex TL = CreateVertex(sinf(angle_H * i) * cosf(current_angle_V + angle_diff) * r, top, cosf(current_angle_V + angle_diff) * cosf(angle_H * i) * r);
			SphericalCoords_UV(TL, angle_H * i, current_angle_V + angle_diff);
			Vertex TR = CreateVertex(sinf(angle_H * j) * cosf(current_angle_V + angle_diff) * r, top, cosf(current_angle_V + angle_diff) * cosf(angle_H * j) * r);
			SphericalCoords_UV(TR, angle_H * j, current_angle_V + angle_diff);


			//Triangulate
			if (BL.pos[0] != BR.pos[0] || BL.pos[1] != BR.pos[1] || BL.pos[2] != BR.pos[2])
			{
				sphere.push_back(BL);
				sphere.push_back(BR);
				sphere.push_back(TL);
				
				n.SetAllNormals(sphere[v++], sphere[v++], sphere[v++]);
			}

			//Triangulate
			if (TL.pos[0] != TR.pos[0] || TL.pos[1] != TR.pos[1] || TL.pos[2] != TR.pos[2])
			{
				sphere.push_back(TR);
				sphere.push_back(TL);
				sphere.push_back(BR);

				n.SetAllNormals(sphere[v++], sphere[v++], sphere[v++]);
			}

			//Calculate the normals
			current_angle_V += angle_diff;
		}
	}

	//Set the colors
	SetColors(sphere);

	Vertex vtx;
	vtx.ComputeTangentBasis(sphere);

	//Calculate the average normals and the light properties
	size_t max = sphere.size();
	for (unsigned i = 0; i < max; i++)
	{
		sphere[i].CalculateAverageVectors(sphere);
	}

	return sphere;
}


/**
* @brief Calculates the vertices of a sphere used for the lights
*
* @return: the vertices that make up the shape
*/
std::vector<Vertex> Shape::DrawLightSphere()
{
	std::vector<Vertex> light_sphere;
	unsigned subdiv = subdiv_sphere;

	//Draw a sphere with 8 subdivisions
	subdiv_sphere = 8;
	light_sphere = DrawSphere(keep);

	//Set original value back
	subdiv_sphere = subdiv;

	//Set color: white
	for (int i = 0; i < light_sphere.size(); i++)
	{
		light_sphere[i].color[0] = 1.f;
		light_sphere[i].color[1] = 1.f;
		light_sphere[i].color[2] = 1.f;
	}


	return light_sphere;
}
