#pragma once

#include "shapes.h"

class MaterialParameters
{
public:
	MaterialParameters();

	float attenuation_color[3]{ 0.f,0.f,0.f };

	float diffuse_color[3] { 1.f, 1.f, 1.f };
	float ambient = 1.f;
	float specular_color[3] { 1.f, 1.f, 1.f };
	float shininess = 10.f;
};

class Lights
{
public:
	MaterialParameters material_p;

	int type = -1;

	float currentPos[3]{ 0.f, 0.f, 0.f };
	float lightColor[3]{ 0.f, 0.f, 0.f };

	float spotDirection[3]{ 0.f, 0.f, 0.f };
	float spotExponent = 0.f;
	float spotCosInner = 0.f;
	float spotCosOuter = 0.f;


	std::vector<Vertex> GetLight();

private:
	std::vector<Vertex> lights_sphere;
};