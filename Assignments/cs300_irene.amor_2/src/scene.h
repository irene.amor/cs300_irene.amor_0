#pragma once

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

#include <GL/glew.h>
#include <GL/GL.h>
#include <SDL2/SDL.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "OGLDebug.h"
#include "CS300Parser.h"
#include "shapes.h"
#include "texturing.h"
#include "lights.h"

#include <fstream>
#include <array>

#include <stb/stb_image.h>


//-----------------------
//Scene
//-----------------------
class ArraysObject
{
public:
	GLuint BufferObjects;	//positions, colors, uv, ambient, diffuse, specular, shininess, face normal, is_white
	GLuint vao;
};


struct NormalMap
{
	char name[50];
	stbi_uc* data = nullptr;
	int width = 0;
	int height = 0;
	NormalMap(const char* n, stbi_uc* d, int w, int h);
};

class Scene
{
public:

	void init();
	void display(SDL_Window* window);
	void cleanup();
	void update();
	void GetInput(SDL_Event event);


	bool quit = false;

private:

	//FUNCTIONS
	//GLuint CreateShader(GLenum eShaderType, const std::string& strShaderFile);
	//GLuint CreateProgram(const std::vector<GLuint>& shaderList);
	
	void InitializeLights();
	//void InitializeShader(GLuint& program);
	void InitializeProgram();
	void InitializeBuffers();
	void InitializeUniforms();
	void SetLightPosUniforms();
	void BufferCleanup();


	glm::mat4 UpdateCameraViewMtx();

	void TransfMatrices(unsigned idx);

	//VARIABLES
	enum {increase, decrease, keep};
	enum {POINT, DIR, SPOT};
	int change_subdiv = keep;
	

	glm::mat4 Projection{ glm::mat4(1.f) };
	std::vector<glm::mat4> transform;
	std::vector<size_t> vtx_count;
	std::vector<size_t> normals_count;


	GLuint theProgram_Tex = 0;
	std::vector<ArraysObject> AO_S;
	std::vector<ArraysObject> AO_N;


	const char* prev_normalMap = nullptr;
	std::vector<NormalMap> normalMaps;
	GLuint normal_map_tex = 0;
	Texture texture;
	Shape shape;
	std::vector<Lights> lights;

	enum rendering_modes { normalMapping, normals, tangents, bitangents, MAX_modes };

	//bool using_texture = false;
	bool wireframe_mode = false;
	bool draw_normals = false;
	bool average_normals = true;
	int rendering_mode = 0;

	CS300Parser parser;
	float alpha = 0.f;
	float beta =  0.f;
	float cam_radius = 0.f;
	float max_cam_radius = 0.f;


};




