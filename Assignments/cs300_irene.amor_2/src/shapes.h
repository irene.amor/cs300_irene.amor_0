#pragma once

#include <vector>
#include <string>
#include <glm/glm.hpp>

//-----------------------
//Shapes
//-----------------------

enum vector_type { faceNormal, faceTangent, faceBitangent, aveNormal, aveTangent, aveBitangent, TOTAL_vec };

class Normal
{
public:

	glm::vec4 vector;
	glm::vec4 P0;
	glm::vec4 P1;
	glm::vec3 color;

	Normal();
};


class Vertex
{
public:
	float pos[4]{ 0.f,0.f,0.f,0.f };
	float color[3]{ 0.f,0.f,0.f };
	unsigned char UV[2]{ 0,0 };

	float diffuse_color[3] = { 1.f, 1.f, 1.f };
	float ambient = 1.f;
	float specular_color[3] = { 1.f, 1.f, 1.f };
	float shininess = 10.f;

	Normal face_normal, face_tangent, face_bitangent;
	Normal ave_normal, ave_tangent, ave_bitangent;
	
	//MaterialParameters* material_p = nullptr;
	float is_white = -1.f;

	void ComputeTangentBasis(std::vector<Vertex>& vertices) const;
	void CheckTangentPerpendicularity(bool average);
	void SetAllNormals(Vertex& P0, Vertex& P1, Vertex& P2);
	void CalculateFaceNormal(Vertex& P1, Vertex& P2);
	void CalculateAverageVectors(std::vector<Vertex> shape);
	void SetVectorsInfo(bool average);
};


class Shape
{
public:
	std::vector<Vertex> DrawShape(std::string mesh, unsigned change_subdiv, float shininess);
	std::vector<Vertex> GetNormals(std::vector<Vertex> shape, bool average);

	std::vector<Vertex> DrawLightSphere();

private:
	Vertex CreateVertex(float x, float y, float z);

	std::vector<Vertex> DrawFromMesh(std::string filename);

	void SetColors(std::vector<Vertex>& shape);

	//void GetFaceNormals(std::vector<Vertex>& shape);
	//void GetAverageNormals(std::vector<Vertex>& shape);

	void CylindricalCoords_UV(Vertex& vtx, float angle);
	void SphericalCoords_UV(Vertex& vtx, float angle_H, float angle_V);

	void CalculatePlane();
	void CalculateCube();

	std::vector<Vertex> DrawPlane();
	std::vector<Vertex> DrawCube();
	std::vector<Vertex> DrawCone(unsigned change_subdiv);
	std::vector<Vertex> DrawCylinder(unsigned change_subdiv);
	std::vector<Vertex> DrawSphere(unsigned change_subdiv);

	std::vector<Vertex> plane, cube;

	enum v { base_center, BR, BL, top_vtx, BR2, BL2 };

	enum { increase, decrease, keep };
	unsigned subdiv_cone = 4;
	unsigned subdiv_cylinder = 4;
	unsigned subdiv_sphere = 4;

	const float r = 0.5f;
	float pi = 0.f;
};

