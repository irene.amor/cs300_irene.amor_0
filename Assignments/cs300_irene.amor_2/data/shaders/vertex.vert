#version 400
layout(location = 0) in vec4 aPosition;
layout(location = 1) in vec3 aColor;
layout(location = 2) in vec2 UV;
layout(location = 3) in vec4 face_normal;
layout(location = 4) in float is_white;
layout(location = 5) in vec4 tangent_vec;
layout(location = 6) in vec4 bitangent_vec;

uniform mat4 MVP;
uniform mat4 M2W;
uniform mat4 viewMtx;

uniform float using_tex;
uniform float rendering_mode;

out vec2 uv;
out vec3 color;
out vec4 fragPos;
out vec4 faceNormal;
out float white;
out vec3 normal;
out vec3 tangent;
out vec3 bitangent;

void main()
{		
	gl_Position	= MVP * aPosition;
		
	color = aColor;
	uv			= UV/255;
	fragPos		= (viewMtx * M2W * vec4(aPosition.xyz, 1.f));				//Vertex position in camera space

	white = is_white;

	normal = (transpose(inverse(viewMtx * M2W)) * face_normal).xyz;
	tangent = (transpose(inverse(viewMtx * M2W)) * tangent_vec).xyz;
	bitangent = (transpose(inverse(viewMtx * M2W)) * bitangent_vec).xyz;



	//Transform the face normal
	if(rendering_mode == 0)
	{
		faceNormal = (transpose(inverse(viewMtx * M2W)) * (face_normal));	//Vertex normal in camera space
		faceNormal.w = 0.f;
	}

	//Set the color as the position of the corresponding tangent vector in camera space
	if(is_white != 1)
	{
		if(rendering_mode == 1)
		{
			vec4 vecColor = (transpose(inverse(viewMtx * M2W)) * vec4(face_normal.xyz, 0.f));
			color = normalize((vecColor).rgb);
		}
		else if(rendering_mode == 2)
		{
			vec4 vecColor = (transpose(inverse(viewMtx * M2W)) * vec4(tangent_vec.xyz, 0.f));

			if(is_white != 1)
				color = normalize((vecColor).rgb);
		}
		else if(rendering_mode == 3)
		{
			vec4 vecColor = (transpose(inverse(viewMtx * M2W)) * vec4(bitangent_vec.xyz, 0.f));

			if(is_white != 1)
				color = normalize((vecColor).rgb);
		}
	}
}