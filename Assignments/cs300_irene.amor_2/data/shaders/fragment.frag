#version 430
in vec3 color;
in vec2 uv;
in vec4 fragPos;
in vec4 faceNormal;
in float white;
in vec3 normal;
in vec3 tangent;
in vec3 bitangent;

layout (location = 3) uniform float ambient;
layout (location = 4) uniform float shininess;
layout (location = 5) uniform float rendering_mode;


layout (location = 0) uniform sampler2D textureData;
layout (location = 1) uniform sampler2D normalTextureData;


uniform vec3 cam_pos;
uniform mat4 viewMtx;

uniform int max_lights;

uniform int l_type[8];				// 0: POINT, 1: DIR, 2: SPOTLIGHT
uniform vec3 l_color[8];
uniform float l_ambient[8];
uniform vec3 l_diffuse[8];
uniform vec3 l_specular[8];
uniform vec3 l_attenuation[8];

uniform vec3 l_direction[8];		//Directional lights
uniform float l_falloff[8];			//Spotlight falloff
uniform float l_cos_inner[8];		//Spotlight: cosine of the inner angle
uniform float l_cos_outer[8];		//Spotlight: cosine of the outer angle

uniform vec3 l_position[8];			//Position of the light in world space
uniform mat4 l_MVP[8];


out vec4 outputColor;

vec3 CalculateLighting(vec4 faceNormal_, vec4 V, vec3 diffuseColor, vec3 specularColor, vec3 textureColor)
{
	
	vec3 I_total = vec3(0.f, 0.f, 0.f);
	
	
	for(int i = 0; i < max_lights; i++)
	{
		//LIGHT info
		vec3 lightColor = vec3(l_color[i][0], l_color[i][1], l_color[i][2]);
		vec3 I_ambient = l_ambient[i] * ambient * lightColor * textureColor;
		vec4 l_pos = viewMtx * vec4(l_position[i], 1.f);
			
		//Direction of the light
		vec4 L = vec4(0.f, 0.f, 0.f, 0.f);
		
		if(l_type[i] == 0 || l_type[i] == 2)
		{
			L = l_pos - fragPos;	//For point light and spotlight
			L.w = 0.f;
			L = normalize(L);
		}
		else	
		{	//For directional light	
			L = normalize(viewMtx * vec4(-l_direction[i], 0.f));
		}
			
		//DIFFUSE component
		vec3 I_diffuse	= l_diffuse[i] * diffuseColor * max(dot(faceNormal_, L),0);


		//SPECULAR component
		vec4 R = normalize(2 * dot(faceNormal_, L) * faceNormal_ - (L));
		vec3 I_specular = l_specular[i] * specularColor * pow(max(dot(R, V),0), shininess);

		
		//ATTENUATION component - Not for directional light
		float Att = 1.f;
		if(l_type[i] == 0 || l_type[i] == 2)
		{
			float dL = distance(l_pos.xyz, fragPos.xyz);
			Att = min((1/(l_attenuation[i][0] + l_attenuation[i][1] * dL + l_attenuation[i][2] * dL * dL)), 1);
		}


		//SPOTLIGHT EFFECT
		float SpotlightEffect = 1.f;
		if(l_type[i] == 2)
		{
			vec4 D = normalize(vec4(-l_direction[i], 0.f));
			L = normalize(l_pos - fragPos);
			//float alpha = acos((dot(normalize(L), normalize(D))));
			float cos_a = dot(normalize(L), normalize(D));
			
			SpotlightEffect = pow( ((cos_a - l_cos_outer[i]) / (l_cos_inner[i] - l_cos_outer[i])), l_falloff[i]);
			SpotlightEffect = clamp(SpotlightEffect, 0, 1);
		}
		
		I_total += I_ambient + Att * (SpotlightEffect * (I_diffuse + I_specular));
	}
 	
	return I_total;
}

void main()
{
	//Normal colors
	if(rendering_mode != 0)
	{
		outputColor = vec4(color, 1.f);
		return;
	}

	//Tangent to camera space matrix
	vec3 N_vec = normalize(normal);
	vec3 T_vec = normalize(tangent);
	vec3 B_vec = normalize(bitangent);
	mat3 TangentMtx = mat3(T_vec, B_vec, N_vec);


	//Normal in camera space
    vec3 N = texture(normalTextureData, uv).rgb;
	N = (2 * N) - 1;

	vec4 faceNormal_ = vec4(TangentMtx * N, 0.f);


	//View vector	
	vec4 V = normalize(vec4(-fragPos.xyz, 0.f));

	vec3 diffuseColor = color;
	vec3 specularColor = vec3(1.f, 1.f, 1.f);



	//Get color for drawing normals and white light spheres
	if(white == 1.f)
	{
		outputColor = vec4(normalize(color), 1.f);
		return;
	}

	//Normal mapping
	else if(rendering_mode == 0)
	{
		vec4 textureColor = texture(textureData, uv);
		vec3 diffuseColor = textureColor.rgb;
		outputColor =  vec4(CalculateLighting(faceNormal_, V, diffuseColor, specularColor, textureColor.rgb), 1.f);
		return;
	}

}